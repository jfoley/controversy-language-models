package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.collections.util.SetFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.parse.stem.KrovetzStemmer;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Controversy Lexicon from:
 * http://arxiv.org/abs/1409.8152
 * Controversy and Sentiment in Online News
 * Yelena Mejova, Amy X. Zhang, Nicholas Diakopoulos, Carlos Castillo
 *
 * @author jfoley
 */
public class ControversyLexiconClassifier2 {
  /** Copied from Table 3. */
  static final Set<String> StronglyControversial = new HashSet<>(Arrays.asList("abuse administration afghanistan aid america american army attack attacks authorities authority ban banks benefits bill bills border budget campaign candidate candidates catholic china chinese church concerns congress conservative control country court crime criminal crisis cuts debate debt defense deficit democrats disease dollar drug drugs economy education egypt election elections enforcement fighting finance fiscal force funding gas government gun health immigration inaccuracies india insurance investigation investigators iran israel job jobs judge justice killing korea labor land law lawmakers laws lawsuit leadership legislation marriage media mexico military money murder nation nations news obama offensive officials oil parties peace police policies policy politics poll power president prices primary prison progress race reform republican republicans restrictions rule rules ruling russia russian school security senate sex shooting society spending strategy strike support syria syrian tax taxes threat trial unemployment union usa victim victims violence vote voters war washington weapons world".split(" ")));
  static final Set<String> SomewhatControversial = new HashSet<>(Arrays.asList(("account advantage amount attorney chairman charge charges cities class comment companies cost credit delays effect expectations families family february germany goal housing information investment markets numbers oklahoma parents patients population price projects raise rate reason sales schools sector shot source sources status stock store worth").split(" ")));
  static final Set<String> NonControversial = new HashSet<>(Arrays.asList(("60s 70s addition address afternoon agreed amp angeles answer april attention avenue average ball base bay beach beginning bit block blue bowl box boy boys brother building bus call calling calls camp car cars central cents click close cloudy club coast cup dallas date daughter davis day decade decades december def delivery door download drive eagles end entire era evening face faces facility fall fans father feel feeling feet fell field finish floor form fort francisco friday friend friends fun girl girls ground gt guy guys half hall hand hands hawaii heart heat heavy hill hits hold hopes host hotel hour hours house houston hundreds husband ice illinois index indiana innings island january johnson jones june kansas kind lack lake leave lee letter levels light line lines lot lows lt main make makes mark mass material matter medium men mid middle miles mind minneapolis minutes moment monday month months morning mother mountain move mph museum names natural net night north note notes november october opening park part parts pass period person philadelphia pick pitch plant play player playing pm point post practice put quarter rain read reading red rest restaurant rise rock rose round sale san saturday scene search season seasons seconds selling september series set showers showing shows sign signs smith son sox special spot spring square stadium stage start starting starts station stay step stores street student summer sun sunday thing things thinking thought thousands thunderstorms thursday time title top total transportation type unit valley vehicle version village visit wait walk wall watch water ways wednesday week weekend weeks williams wind winds winner winter word writer yards year years york").split(" ")));

  public static class SaveAsQrel {
    public static void main(String[] args) throws IOException {
      try (PrintWriter qrel = IO.openPrintWriter("mejova.qrel")) {
        for (String doc : StronglyControversial) {
          qrel.printf("controversy skip %s 2\n", doc);
        }
        for (String doc : SomewhatControversial) {
          qrel.printf("controversy skip %s 1\n", doc);
        }
        for (String doc : NonControversial) {
          qrel.printf("controversy skip %s 0\n", doc);
        }
      }
    }
  }

  private static class HyperParam {
    final int lambda1;
    final int lambda2;
    final int lambda3;
    final double score;
    final double cutoff;

    public HyperParam(HyperParam original, double score, double cutoff) {
      this.lambda1 = original.lambda1;
      this.lambda2 = original.lambda2;
      this.lambda3 = original.lambda3;
      this.score = score;
      this.cutoff = cutoff;
    }

    public HyperParam(int lambda1, int lambda2, int lambda3) {
      this.lambda1 = lambda1;
      this.lambda2 = lambda2;
      this.lambda3 = lambda3;
      this.score = 0;
      this.cutoff = 0;
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof HyperParam) {
        HyperParam rhs = (HyperParam) other;
        return this.lambda1 == rhs.lambda1 && this.lambda2 == rhs.lambda2 && this.lambda3 == rhs.lambda3;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return Integer.hashCode(this.lambda1) ^ Integer.hashCode(this.lambda2) ^ Integer.hashCode(this.lambda3);
    }

    double l1() {
      return lambda1 / 100.0;
    }

    double l2() {
      return lambda2 / 100.0;
    }

    double l3() {
      return lambda3 / 100.0;
    }

    @Override
    public String toString() {
      return String.format("HyperParam(lambda=(%1.2f, %1.2f, %1.2f))", l1(), l2(), l3());
    }
  }

  /**
   * TODO: This needs to train a way to combine the three lexicons.
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    //argp.set("dataset", "jwpl-data");
    //argp.set("dataset", "/home/jfoley/code/controversyNews/data/mhjang2016/release");
    //argp.set("datasetName", "web450");
    argp.set("datasetName", "web150");
    //argp.set("datasetName", "clue303");
    ControversyDataset dataset = new ControversyDataset(argp);

    String outputDir = argp.get("outputDir", "output"+argp.get("datasetName", ""));
    Directory out = new Directory(outputDir); // create if needed.
    boolean tuneAccuracy = argp.get("tuneAccuracy", false); // else tune AUC


    TagTokenizer tokenizer = new TagTokenizer();
    KrovetzStemmer stemmer = new KrovetzStemmer();

    final Set<String> stronglyK = new HashSet<>();
    final Set<String> somewhatK = new HashSet<>();
    final Set<String> nonK = new HashSet<>();
    for (String term : StronglyControversial) {
      stronglyK.add(stemmer.stem(term));
    }
    for (String term : SomewhatControversial) {
      somewhatK.add(stemmer.stem(term));
    }
    for (String term : NonControversial) {
      nonK.add(stemmer.stem(term));
    }

    List<Integer> lambdas = new ArrayList<>(Arrays.asList(
        -100,-50,-40,-30,-20,-10,-5,0,5,10,20,30,40,50,100
    ));


    // calculate all predictions for all parameter settings, and then do search afterward.
    Map<HyperParam, Map<String, Double>> allPredictions = new HashMap<>();

    dataset.fullText.forEach((id, text) -> {
      Set<String> docSet = new HashSet<>(ListFns.map(tokenizer.tokenize(text).terms, stemmer::stem));
      // three features here:
      double strongPred = SetFns.jaccardIndex(docSet, stronglyK);
      double somewhatPred = SetFns.jaccardIndex(docSet, somewhatK);
      double nonPred = SetFns.jaccardIndex(docSet, nonK);

      for (int l1 : lambdas) {
        for (int l2 : lambdas) {
          for (int l3 : lambdas) {
            HyperParam hp = new HyperParam(l1, l2, l3);

            final double score =
                hp.l1() * strongPred +
                hp.l2() * somewhatPred +
                hp.l3() * nonPred;
            allPredictions
                .computeIfAbsent(hp, missing -> new HashMap<>())
                .put(id, score);
          }
        }
      }
    });


    // Start search for best parameter settings per-split:
    Parameters saved = Parameters.create();
    Map<String, Double> testPredictions = new HashMap<>();

    for (ControversyDataset.DatasetSplit split : dataset.splits) {
      List<HyperParam> trainedValues = new ArrayList<>();
      //List<Pair<Integer,Double>> depthAUCPairs = new ArrayList<>();
      for (HyperParam hp : allPredictions.keySet()) {
        Map<String, Double> scores = allPredictions.get(hp);
        if(scores.isEmpty()) continue;
        List<Pair<Boolean, Double>> toEval = new ArrayList<>();
        for (String clueId : split.train) {
          if(!dataset.judgments.containsKey(clueId)) continue;
          Double score = scores.get(clueId);
          if(score == null) {
            System.err.println(split.id+" NULL: "+clueId);
            continue;
          }
          boolean judgment = dataset.judgments.get(clueId).isControversial();
          toEval.add(Pair.of(judgment, score));
        }
        double thisCutoff = tuneAccuracy ? AUC.maximizeAccuracy(toEval) : AUC.maximizeF1(toEval);
        double thisEval = tuneAccuracy ? BinaryClassifierInfo.computeAccuracy(toEval, thisCutoff) : AUC.compute(toEval);
        trainedValues.add(new HyperParam(hp, thisEval, thisCutoff));
      }

      // find the best hyper-parameters
      trainedValues.sort((lhs, rhs) -> -Double.compare(lhs.score, rhs.score));

      if(argp.get("showBestK", true)) {
        System.out.println("Best-K:");
        for (HyperParam hyperParam : ListFns.take(trainedValues, 10)) {
          System.out.println("\t" + hyperParam + " AUC=" + hyperParam.score);
        }
      }

      HyperParam best = trainedValues.get(0);
      double bestScore = best.score;
      double cutoff = best.cutoff;
      System.out.println(split.id+" best " +(tuneAccuracy ? "ACC" : "AUC")+ ": "+bestScore+" "+best.toString());

      // save deep information for learning to rank approach.
      Parameters trainP = Parameters.create();
      Parameters testP = Parameters.create();
      for (String clueId : split.test) {
        double value = allPredictions.get(best).get(clueId) - cutoff;
        testP.put(clueId, value);
        testPredictions.put(clueId, value);
      }
      for (String clueId : split.train) {
        trainP.put(clueId, allPredictions.get(best).get(clueId));
      }
      saved.put(split.id, Parameters.parseArray(
          "lambda1", best.lambda1,
          "lambda2", best.lambda2,
          "lambda3", best.lambda3,
          "cutoff", best.cutoff,
          (tuneAccuracy ? "ACC" : "AUC"), best.score,
          "train", trainP, "test", testP
      ));
    }
    System.out.println("Tuned all folds.");

    IO.spit(saved.toString(), out.child("lexicon2.trained..json"));

    List<Pair<Boolean, Double>> forAUC = new ArrayList<>();
    BinaryClassifierInfo naiveBayes = new BinaryClassifierInfo();
    try (PrintWriter scores = IO.openPrintWriter(out.childPath("/lexicon2.trained..tsv"))) {
      for (Map.Entry<String, Double> kv : testPredictions.entrySet()) {
        String clueId = kv.getKey();
        if(!dataset.judgments.containsKey(clueId)) continue;
        double prediction = kv.getValue();
        boolean expected = dataset.judgments.get(clueId).isControversial();
        scores.println(clueId + "\t" + prediction);
        forAUC.add(Pair.of(expected, prediction));
      }
    }
    naiveBayes.update(forAUC, 0); // cutoff should have been subtracted off
    System.out.println("TEST AUC: "+AUC.compute(forAUC));
    System.out.println("NAIVE-BAYES: "+naiveBayes);

  }
}
