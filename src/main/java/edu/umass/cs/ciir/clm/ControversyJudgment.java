package edu.umass.cs.ciir.clm;

/**
 * A Controversy Judgment as created by Dori-Hacohen et al. It is controversial if the average judgment is below 2.5.
 *
 * TRUE:
 * 1 - clearly controversial
 * 2 - possibly controversial
 *
 * FALSE:
 * 3 - possibly non-controversial
 * 4 - clearly non-controversial
 *
 * @author jfoley
 */
public class ControversyJudgment {
  public final String key;
  public String annotator = null;
  public final double averageControversyJudgment;

  public ControversyJudgment(String key, double averageControversyJudgment) {
    this.key = key;
    this.averageControversyJudgment = averageControversyJudgment;
  }

  public double toRelevanceLikeRating() {
    return 3 - averageControversyJudgment; // C:2,1, NC:0,-1
  }

  @Override
  public String toString() {
    return String.format("%1.1f", toRelevanceLikeRating());
  }

  /**
   * Allow creation from boolean truth value.
   * @param key document id
   * @param isControversial controversial label
   */
  public ControversyJudgment(String key, boolean isControversial) {
    this.key = key;
    this.averageControversyJudgment = (isControversial ? 1.0 : 4.0);
  }

  /**
   * This whole class exists so I don't have to remember this:
   * @return true or false!
   */
  public boolean isControversial() {
    return averageControversyJudgment < 2.5;
  }
}
