package edu.umass.cs.ciir.clm.sim;

import ciir.jfoley.chai.collections.Pair;
import edu.umass.cs.ciir.clm.ControversyJudgment;

import java.util.List;
import java.util.Map;

/**
 * @author jfoley
 */
public class AvgTruthYesPred extends Simulation {
  @Override
  protected void setupSimulation(Map<String, List<ControversyJudgment>> dataToTest) {

  }

  @Override
  protected Pair<Boolean, Double> simulatePrediction(String key, List<ControversyJudgment> values) {
    return Pair.of(this.avgTruth(values), 2.0);
  }
}
