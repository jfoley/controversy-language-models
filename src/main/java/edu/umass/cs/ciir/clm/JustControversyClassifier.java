package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.io.IO;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.parse.stem.KrovetzStemmer;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class JustControversyClassifier {

 public static void main(String[] args) throws IOException {
  Parameters argp = Parameters.parseArgs(args);
  //argp.set("dataset", "jwpl-data");
  //argp.set("dataset", "/home/jfoley/code/controversyNews/data/mhjang2016/release");
  //argp.set("datasetName", "web150");
  //argp.set("datasetName", "clue303");
  argp.put("dhaFilter", "/home/jfoley/code/controversyNews/titlesHodaWithTalk.txt");
  argp.put("dataset", "jwpl-data");

  ControversyDataset dataset = new ControversyDataset(argp);
  TagTokenizer tokenizer = new TagTokenizer();
  KrovetzStemmer stemmer = new KrovetzStemmer();

  List<Pair<Boolean, Double>> preds = new ArrayList<>();
  try (PrintWriter pw = IO.openPrintWriter("just-controversy-classifier.tsv")) {
   dataset.fullText.forEach((id, text) -> {
    int count = 0;
    List<String> doc = tokenizer.tokenize(text).terms;
    for (String term : doc) {
     if("controversy".equals(term)) {
      count++;
     }
    }
    double pred = count / (double)doc.size();
    pw.println(id +"\t"+pred);
    preds.add(Pair.of(dataset.judgments.get(id).isControversial(), pred));
   });

   BinaryClassifierInfo info = new BinaryClassifierInfo();
   info.update(preds, 0);
   System.out.println("AUC="+AUC.compute(preds));
   System.out.println(info);
  }
 }
}
