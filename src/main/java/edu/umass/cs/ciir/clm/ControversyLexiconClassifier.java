package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.collections.util.SetFns;
import ciir.jfoley.chai.io.IO;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.lemurproject.galago.core.eval.EvalDoc;
import org.lemurproject.galago.core.eval.QueryResults;
import org.lemurproject.galago.core.eval.QuerySetResults;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.parse.stem.KrovetzStemmer;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Controversy Lexicon from:
 *
 * <a href="http://dl.acm.org/citation.cfm?id=2891115">
 *   Roitman, Haggai, et al.
 *   "On the Retrieval of Wikipedia Articles Containing Claims on Controversial Topics."
 *   WWW, 2016.
 * </a>
 *
 * @author jfoley
 */
public class ControversyLexiconClassifier {
 public static final String lexiconWords = "dispute disputable disagreement debate polemic feud question schism wrangle controversy dispeace dissension criticism argue disagree argument claim conflict opposition adversary antagonism oppose object loggerheads quarrel fuss moot hassle altercate case evidence clash issue problem emphasize recommend suggest assert defend maintain reject support challenge doubt refute confirm prove validate establish substantiate verify against resist support agree consent concur accept refuse plead right justify justification";
 /** Copied from Table 3. */
 static final Set<String> ManualControversyLexicon = new HashSet<>(Arrays.asList(lexiconWords.split(" ")));

 public static class SaveAsQrel {
  public static void main(String[] args) throws IOException {
   try (PrintWriter qrel = IO.openPrintWriter("roitman.qrel")) {
    for (String doc : ManualControversyLexicon) {
     qrel.printf("controversy skip %s 1\n", doc);
    }
   }
  }
 }

 public static class RunWithOurLMWeights {
  public static void main(String[] args) throws IOException {
   Parameters argp = Parameters.parseArgs(args);
   argp.set("datasetName", "web150");
   //argp.set("datasetName", "clue303");
   ControversyDataset dataset = new ControversyDataset(argp);

   TagTokenizer tokenizer = new TagTokenizer();
   KrovetzStemmer stemmer = new KrovetzStemmer();


   QuerySetResults qsr = new QuerySetResults(argp.get("languageModelWeightTrecrun", "dbpedia.irene.logodds.trecrun"));
   TObjectDoubleHashMap<String> lmWeights = new TObjectDoubleHashMap<>();
   QueryResults controversy = qsr.toMap().get("controversy");
   for (EvalDoc evalDoc : controversy) {
    String stem = stemmer.stem(evalDoc.getName());
    lmWeights.put(stem, evalDoc.getScore());
   }

   double background = 1e-10; // classified as positive, but probably less than every other one.

   HashMap<String, Double> lexiconScores = new HashMap<>();
   for (String term : ManualControversyLexicon) {
    String stem = stemmer.stem(term);
    double weight = background;
    if(lmWeights.containsKey(stem)) {
     weight = lmWeights.get(stem);
    } else if(lmWeights.containsKey(term)) {
     weight = lmWeights.get(term);
    }
    lexiconScores.put(stem, weight);
   }

   List<Pair<Boolean, Double>> preds = new ArrayList<>();
   try (PrintWriter pw = IO.openPrintWriter("controversy-lexicon-lm.tsv")) {
    dataset.fullText.forEach((id, text) -> {
     Set<String> docSet = new HashSet<>(ListFns.map(tokenizer.tokenize(text).terms, stemmer::stem));
     double pred = 0;
     for (String s : docSet) {
      if(lexiconScores.containsKey(s)) {
       pred += lexiconScores.getOrDefault(s, background);
      }
     }
     //double pred = SetFns.jaccardIndex(docSet, kstemmmedLexicon);
     preds.add(Pair.of(dataset.judgments.get(id).isControversial(), pred));
     pw.println(id + "\t" + pred);
    });

    System.out.println("AUC="+AUC.compute(preds));
   }

  }
 }


 public static void main(String[] args) throws IOException {
  Parameters argp = Parameters.parseArgs(args);
  //argp.set("dataset", "jwpl-data");
  //argp.set("dataset", "/home/jfoley/code/controversyNews/data/mhjang2016/release");
  argp.set("datasetName", "web150");
  //argp.set("datasetName", "clue303");
  ControversyDataset dataset = new ControversyDataset(argp);
  TagTokenizer tokenizer = new TagTokenizer();
  KrovetzStemmer stemmer = new KrovetzStemmer();

  final Set<String> kstemmmedLexicon = new HashSet<>();
  for (String term : ManualControversyLexicon) {
   kstemmmedLexicon.add(stemmer.stem(term));
  }

  List<Pair<Boolean, Double>> preds = new ArrayList<>();
  try (PrintWriter pw = IO.openPrintWriter("controversy-lexicon-classifier.tsv")) {
   dataset.fullText.forEach((id, text) -> {
    Set<String> docSet = new HashSet<>(ListFns.map(tokenizer.tokenize(text).terms, stemmer::stem));
    double pred = SetFns.jaccardIndex(docSet, kstemmmedLexicon);
    preds.add(Pair.of(dataset.judgments.get(id).isControversial(), pred));
    pw.println(id + "\t" + pred);
   });

   System.out.println("AUC="+AUC.compute(preds));
  }
 }
}
