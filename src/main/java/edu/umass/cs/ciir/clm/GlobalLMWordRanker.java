package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.collections.TopKHeap;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.math.StreamingStats;
import ciir.jfoley.chai.string.StrUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.util.WordLists;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author jfoley
 */
public class GlobalLMWordRanker {
  protected final Parameters argp;
  public ControversyDataset dataset;
  public Set<String> stopwords;

  public GlobalLMWordRanker(Parameters argp) throws IOException {
    this.argp = argp;
    this.dataset = new ControversyDataset(argp);
    this.stopwords = WordLists.getWordListOrDie("smart");
  }

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    GlobalLMWordRanker exp = new GlobalLMWordRanker(argp);
    exp.run();
  }

  public DocumentStore openIndex(String path) {
    try {
      return new DocumentStore.GalagoDocumentStore(new LocalRetrieval(path, argp));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void run() throws IOException {
    //argp.set("dataset", "/home/jfoley/code/controversyNews/");
    String wikiSourceIndex = argp.get("wiki", "/mnt/scratch3/jfoley/dbpedia.galago");
    String method = argp.get("method", "mscore");
    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/clue09bspam60.galago");
    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/dbpedia.galago");

    // directory that contains "MScore.txt", "CScore.txt", and "DScore.txt"
    Directory rawData = Directory.Read(argp.get("features", "/home/jfoley/code/controversyNews/clueweb"));
    String indexPath = argp.get("index", (!method.startsWith("index") ? wikiSourceIndex : StrUtil.takeAfter(method, ":")));

    final int depth = argp.get("depth", 100);
    final double lambda = 0.8;

    String fileName = method;
    if(method.startsWith("index")) {
      fileName = StrUtil.takeBefore(new File(indexPath).getName(), ".galago");
      String query = argp.get("query", "controversy");
      if(!"controversy".equals(query)) {
        fileName += "." + StrUtil.preview(query,50).replaceAll("\\s+", "_");
      }
    }

    List<String> ids = new ArrayList<>(depth);

    List<TopKHeap.Weighted<String>> weightedTerms = new ArrayList<>();

    try (DocumentStore index = openIndex(indexPath)) {
      LanguageModel.HashMapLanguageModel controversy = new LanguageModel.HashMapLanguageModel();
      collectLanguageModelDocuments(index, depth, method, rawData, ids);
      System.out.println("Collected all LM Docs.");
      createLanguageModel(stopwords, ids, controversy, index);
      System.out.println("Created LM.");


      double collectionLength = index.getTotalWeight();
      double length = controversy.getTotalWeight();

      controversy.getCounts().forEachEntry((term, tf) -> {
        if(tf < 3) return true;
        if(term.length() <= 3 || stopwords.contains(term)) return true;
        double cf = index.getTermWeight(term);
        double bgProb = cf / collectionLength;
        double prob = (lambda) * (tf / length) + (1-lambda) * bgProb;

        double logOdds = Math.log(prob) - Math.log(bgProb);
        //termScores.println(term+"\t"+logOdds);
        weightedTerms.add(new TopKHeap.Weighted<>(logOdds, term));
        return true;
      });
    }

    weightedTerms.sort((lhs, rhs) -> -Double.compare(lhs.weight, rhs.weight));
    try (PrintWriter termScores = IO.openPrintWriter(fileName+".logodds.trecrun")) {
      for (int i = 0; i < weightedTerms.size(); i++) {
        TopKHeap.Weighted<String> wt = weightedTerms.get(i);
        termScores.printf("controversy Q0 %s %d %10.8f %s\n", wt.object, i+1, wt.weight, fileName);
      }
    }
  }


  /**
   * Actually calculate the log-odds score for a query string.
   * @param stopwords stopwords to skip
   * @param lambda smoothing param
   * @param contrLanguageModel positive language model (hash map)
   * @param index background/general/non-controversial language model as Galago Index
   * @param query text to tokenize & score
   * @return log odds (log P(C|d) - log P(NC | d))
   */
  protected static double logOddsScore(Set<String> stopwords, double lambda, LanguageModel contrLanguageModel, LanguageModel index, String query) {
    TagTokenizer tok = new TagTokenizer();
    List<String> qterms = new ArrayList<>(new HashSet<>(tok.tokenize(query).terms));

    double collectionLength = index.getTotalWeight();
    double length = contrLanguageModel.getTotalWeight();

    double cdProb = 0;
    double bgdProb = 0;
    for (String qterm : qterms) {
      if (stopwords.contains(qterm) || qterm.length() <= 3) continue;
      double tf = contrLanguageModel.getTermWeight(qterm);
      double cf = index.getTermWeight(qterm);
      if (cf == 0) continue;
      double bgProb = cf / collectionLength;

      double controvProb = lambda * (tf / length) + (1 - lambda) * bgProb;
      assert (!Double.isNaN(controvProb));
      assert (!Double.isNaN(bgProb));
      cdProb += Math.log(controvProb);
      bgdProb += Math.log(bgProb);
      assert (!Double.isNaN(cdProb));
      assert (!Double.isNaN(bgdProb));
    }
    return (cdProb - bgdProb);
  }



  Cache<String, TObjectIntHashMap<String>> docCache = Caffeine.newBuilder().maximumSize(100_000).build();

  /**
   * Stick into "contrLanguageModel" terms from all documents in list "ids" from index "index" excepting the "stopwords".
   * @param stopwords
   * @param ids
   * @param contrLanguageModel
   * @param index
   * @throws IOException
   */
  private void createLanguageModel(Set<String> stopwords, List<String> ids, LanguageModel.Builder contrLanguageModel, DocumentStore index) throws IOException {
    final HashSet<String> missing = new HashSet<>();
    for (String docName : ids) {

      TObjectIntHashMap<String> docLM = docCache.get(docName, docN -> {
        List<String> terms = index.lookupTermsForDocument(docN);

        TObjectIntHashMap<String> thisDocLM = new TObjectIntHashMap<>();

        if(terms.size() == 0) {
          missing.add(docN);
          return thisDocLM;
        }

        for (String term : terms) {
          if (stopwords.contains(term) || term.length() <= 3) continue;
          thisDocLM.adjustOrPutValue(term, 1, 1);
        }

        return thisDocLM;
      });

      docLM.forEachEntry((k, count) -> {
        contrLanguageModel.increment(k, count);
        return true;
      });
    }
    System.out.println("Missing: " + missing.size()+" "+missing);
  }

  /**
   * Run a query, or collect M, D, or C scores for up to depth documents.
   * @param index galago index to run queries on (or wiki index if method doesn't start with index:
   * @param depth the K number of top documents to take, returned in order
   * @param method  the method by which to collect scores, try "mscore", "dscore", or "cscore"
   * @param rawData the directory which contains "MScore.txt", "DScore.txt", or "CScore.txt" as needed.
   * @param ids the list to save the wiki or index ids to in rank order.
   * @throws IOException
   */
  private void collectLanguageModelDocuments(DocumentStore index, int depth, String method, Directory rawData, List<String> ids) throws IOException {
    if(method.startsWith("index:")) {
      ids.addAll(index.findDocumentsForQuery(argp.get("model", "combine"), argp.get("query", "controversy"), depth));
      return;
    }

    switch(method) {
      case "mscore": {
        TopKHeap<TopKHeap.Weighted<String>> topMScores = new TopKHeap<>(depth);
        System.out.println("Start M");
        int above_84930 = 0;
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("MScore.txt"))) {
          for (String line : lines) {
            String[] data = line.split("\t");
            if(data.length < 2) continue;

            double score = Double.parseDouble(data[0]);
            if(score <= 0) continue;
            if(score >= 84930) above_84930++;

            String title = data[1].trim();
            topMScores.offer(new TopKHeap.Weighted<>(score, title));
          }
          System.out.println("for M=84930, K="+above_84930);
        }
        System.out.println("Total Non-Zero M-Score: "+topMScores.getTotalSeen());
        StreamingStats mscoreInfo = new StreamingStats();
        for (TopKHeap.Weighted<String> wt : topMScores.getSorted()) {
          ids.add(wt.object);
          mscoreInfo.push(wt.weight);
        }
        System.out.println(mscoreInfo);

      } break;
      case "dscore": {
        System.out.println("Start D");
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("DScore.txt"))) {
          for (String line : lines) {
            ids.add(line.trim());
          }
        }
        System.out.println("Total D-Score: "+ids.size());
      } break;
      case "cscore": {
        TopKHeap<TopKHeap.Weighted<String>> topCScores = new TopKHeap<>(depth);
        System.out.println("Start C");
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("CScore.txt"))) {
          for (String line : lines) {
            String[] data = line.split("\t");
            if(data.length < 2) continue;

            try {
              String title = data[0].trim();
              double score = Double.parseDouble(data[1]);
              if (score <= 0) continue;

              topCScores.offer(new TopKHeap.Weighted<>(score, title));
            } catch (NumberFormatException nfe) {
              System.err.println(line);
            }
          }
        }
        System.out.println("Total Non-Zero C-Score: "+topCScores.getTotalSeen());
        for (TopKHeap.Weighted<String> wt : topCScores.getSorted()) {
          ids.add(wt.object);
        }
      }
      break;
      default: throw new UnsupportedOperationException(method);
    }
  }

}
