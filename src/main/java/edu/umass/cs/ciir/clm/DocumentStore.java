package edu.umass.cs.ciir.clm;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

/**
 * @author jfoley
 */
public interface DocumentStore extends LanguageModel, Closeable {

  List<String> lookupTermsForDocument(String id);
  List<String> findDocumentsForQuery(String model, String query, int depth);

  class GalagoDocumentStore implements DocumentStore {
    final LocalRetrieval index;
    Cache<String, Long> termCache = Caffeine.newBuilder().maximumSize(100_000).build();
    private final double totalWeight;

    public GalagoDocumentStore(LocalRetrieval index) {
      this.index = index;
      this.totalWeight = getCollectionLength();
    }

    @Override
    public List<String> lookupTermsForDocument(String id) {
      Set<String> altNames = new HashSet<>();
      generateCapitalizedVariants(id, altNames);

      List<String> terms = new ArrayList<>();
      try {
        Map<String, Document> documents = index.getDocuments(new ArrayList<>(altNames), Document.DocumentComponents.JustTerms);
        for (Document document : documents.values()) {
          terms.addAll(document.terms);
        }
      } catch (IOException e) {
        return Collections.emptyList();
      }
      return terms;
    }

    @Override
    public List<String> findDocumentsForQuery(String model, String query, int depth) {
      TagTokenizer tok = new TagTokenizer();
      Node gq = new Node(model);
      gq.addTerms(tok.tokenize(query).terms);

      Results results = index.transformAndExecuteQuery(gq, Parameters.parseArray("requested", depth));

      ArrayList<String> ids = new ArrayList<>(results.scoredDocuments.size());
      for (ScoredDocument sdoc : results.scoredDocuments) {
        ids.add(sdoc.documentName);
      }
      return ids;
    }

    private double getCollectionLength() {
      try {
        return index.getCollectionStatistics(new Node("lengths")).collectionLength;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    private long getTermFrequency(String qterm) {
      try {
        return index.getNodeStatistics(new Node("counts", qterm)).nodeFrequency;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    @Override
    public void close() throws IOException {
      this.index.close();
    }

    @Override
    public double getTermWeight(String term) {
      return termCache.get(term, this::getTermFrequency);
    }

    @Override
    public double getTotalWeight() {
      return totalWeight;
    }
  }

  /**
   * From lower-cased or mix-cased wiki names, generate:
   *
   * aa_ba_ca ->
   * cap1 = Aa_ba_ca
   * capAll = Aa_Ba_Ca
   *
   * @param input the input, maybe uncapitalized string.
   * @param output where to stick the variants
   */
  public static void generateCapitalizedVariants(String input, Collection<String> output) {
    StringBuilder cap1 = new StringBuilder();
    StringBuilder capAll = new StringBuilder();

    char[] charArray = input.toCharArray();
    for (int i = 0; i < charArray.length; i++) {
      char c = charArray[i];
      if(i == 0) {
        cap1.append(Character.toUpperCase(c));
        capAll.append(Character.toUpperCase(c));
      } else {
        cap1.append(c);
        if(charArray[i-1] == '_') {
          capAll.append(Character.toUpperCase(c));
        } else {
          capAll.append(c);
        }
      }
    }

    output.add(input);
    output.add(cap1.toString());
    output.add(capAll.toString());
  }
}
