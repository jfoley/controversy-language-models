package edu.umass.cs.ciir.clm.sim;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.random.Sample;
import edu.umass.cs.ciir.clm.ControversyJudgment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jfoley
 */
public class LabelConflationModel extends Simulation {
  Map<Double, List<ControversyJudgment>> conflationModel;

  @Override
  protected void setupSimulation(Map<String, List<ControversyJudgment>> dataToTest) {
    conflationModel = new HashMap<>(); // label -> possible predictions

    // collect
    dataToTest.forEach((key, values) -> {
      if(values.size() <= 1) return;
      for (ControversyJudgment cj : values) {
        double v = cj.toRelevanceLikeRating();

        // add all items to list in map:
        List<ControversyJudgment> js = conflationModel.getOrDefault(v, new ArrayList<>());
        js.addAll(values);
        conflationModel.put(v, js);
      }
    });

    // Now our model maps from a label to all the possible conflations of that label observed in the dataset.
  }

  @Override
  protected Pair<Boolean, Double> simulatePrediction(String key, List<ControversyJudgment> values) {
    ControversyJudgment truth = Sample.once(values, rand);
    ControversyJudgment prediction = Sample.once(conflationModel.get(truth.toRelevanceLikeRating()), rand);
    return Pair.of(truth.isControversial(), prediction.toRelevanceLikeRating());
  }
}
