package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.IntRange;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.TopKHeap;
import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.collections.util.SetFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.math.StreamingStats;
import ciir.jfoley.chai.random.ReservoirSampler;
import ciir.jfoley.chai.string.StrUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.util.WordLists;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author jfoley
 */
public class KGlobalLanguageModelExperiment {
  public static List<Integer> depths = new ArrayList<>(Arrays.asList(1,5,10,25,50,75,100,150,200,250,1000,2000,2500,3000,4000,5000,7500)); //,85000));
  public static List<Integer> weights = Arrays.asList(0,10,20,30,40,50,60,70,80,90,100);
  public static List<Integer> lambdas = Arrays.asList(10,20,30,40,50,60,70,80,90); // fast lambda selections
  public static List<String> methods = Arrays.asList(
      "max", "min", "mean",
      "exp-max", "exp-min", "exp-mean"
      //"mix-max", "mix-min", "mix-mean"
  );


  protected final Parameters argp;
  public ControversyDataset dataset;
  public Set<String> stopwords;

  public KGlobalLanguageModelExperiment(Parameters argp) throws IOException {
    this.argp = argp;
    this.dataset = new ControversyDataset(argp);
    this.stopwords = WordLists.getWordListOrDie("smart");
  }

  /**
   * Class that lets us store a "lambda", and a "K" as a key (stores measure value and cutoff if need be, but doesn't hash them).
   * So we can store documents and language models for each K,lambda param.
   */
  public static class HyperParam {
    final int depth;
    final int lambda;
    final int numTopics;
    final String topicsMethod;
    final String method;
    final double score;
    final double cutoff;

    public HyperParam(HyperParam original, double score, double cutoff) {
      this.depth = original.depth;
      this.numTopics = original.numTopics;
      this.topicsMethod = original.topicsMethod;
      this.lambda = original.lambda;
      this.method = original.method;
      this.score = score;
      this.cutoff = cutoff;
    }

    public HyperParam(int depth, int numTopics, String topicsMethod, int lambda, String method) {
      this.depth = depth;
      this.numTopics = numTopics;
      this.topicsMethod = topicsMethod;
      this.lambda = lambda;
      this.method = method;
      this.score = 0;
      this.cutoff = 0;
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof HyperParam) {
        HyperParam rhs = (HyperParam) other;
        return this.depth == rhs.depth && this.numTopics == rhs.numTopics && this.lambda == rhs.lambda && Objects.equals(this.method, rhs.method) && Objects.equals(this.topicsMethod, rhs.topicsMethod);
      }
      return false;
    }

    @Override
    public int hashCode() {
      return Integer.hashCode(this.depth) ^ Integer.hashCode(this.lambda) ^ Integer.hashCode(this.numTopics);
    }

    @Override
    public String toString() {
      return String.format("HyperParam(depth=%d, topics=%s(%d), lambda=%1.2f, method=%s)", depth, topicsMethod, numTopics, lambda/100.0, method);
    }
  }

  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);
    KGlobalLanguageModelExperiment exp = new KGlobalLanguageModelExperiment(argp);
    exp.run();
  }

  public DocumentStore openIndex(String path) {
    try {
      return new DocumentStore.GalagoDocumentStore(new LocalRetrieval(path, argp));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void run() throws IOException {
    //argp.set("dataset", "/home/jfoley/code/controversyNews/");
    String wikiSourceIndex = argp.get("wiki", "/mnt/scratch3/jfoley/dbpedia.galago");
    String outputDir = argp.get("outputDir", "output_"+dataset.getName());
    Directory out = new Directory(outputDir); // create if needed.

    for (ControversyDataset.DatasetSplit split : dataset.splits) {
      System.out.println(split);
    }

    String controversialSelectionMethod = argp.get("method", "mscore");
    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/clue09bspam60.galago");
    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/dbpedia.galago");

    // directory that contains "MScore.txt", "CScore.txt", and "DScore.txt"
    Directory rawData = Directory.Read(argp.get("features", "/home/jfoley/code/controversyNews/clueweb"));
    final boolean fullDocQuery = argp.get("full", false);

    boolean tuneAccuracy = argp.get("tuneAccuracy", false); // else tune AUC

    String indexPath = argp.get("index", (!controversialSelectionMethod.startsWith("index") ? wikiSourceIndex : StrUtil.takeAfter(controversialSelectionMethod, ":")));

    String fileName = controversialSelectionMethod;
    if(controversialSelectionMethod.startsWith("index")) {
      fileName = StrUtil.takeBefore(new File(indexPath).getName(), ".galago");
      String query = argp.get("query", "controversy");
      if(!"controversy".equals(query)) {
        fileName += "." + query;
      }
    }
    if(fullDocQuery) {
      fileName += ".full";
    }
    if(tuneAccuracy) {
      fileName += ".acc";
    }

    // first, collect all documents up to max depth.
    int maxDepth = depths.stream().mapToInt(x -> x).max().getAsInt();
    List<String> ids = new ArrayList<>(maxDepth);

    // calculate all predictions for all parameter settings, and then do search afterward.
    Map<HyperParam, Map<String, Double>> allPredictions = new HashMap<>();

    ConcurrentHashMap<String, List<String>> cachedQuery = new ConcurrentHashMap<>();

    try (DocumentStore index = openIndex(indexPath)) {
      collectLanguageModelDocuments(index, maxDepth, controversialSelectionMethod, rawData, ids);
      System.out.println("Collected all LM Docs.");

      // now, predict for all depths:
      for (int depth : depths) {
        int NCDocs = Math.min(depth, ids.size()); // only have as many topics as there are pages for now:
        if(NCDocs == 0) continue;
        List<LanguageModel.Builder> controversyModels = new ArrayList<>(NCDocs);
        LanguageModel.Builder globalCLM = new LanguageModel.HashMapLanguageModel();
        for (int i = 0; i < NCDocs; i++) {
          // each one becomes a single language model
          LanguageModel.Builder clm = new LanguageModel.HashMapLanguageModel(getDocLM(ids.get(i), index));
          globalCLM.increment(clm);
          controversyModels.add(clm);
        }

        for (int numTopics : depths) {
          if(numTopics > NCDocs) {
            break;
          }
          // limit complexity of this setup
          if(numTopics > 50) break;
          for (int x : IntRange.exclusive(0, 5)) {
            int seed = x*1317;
            Random rand = new Random(seed);
            // Only support topics <= NCDocs
            List<LanguageModel.Builder> kmeansTopics = createKMeansTopics(rand, numTopics, NCDocs, controversyModels);
            System.out.println("\tCreated K="+numTopics+" topics with KMeans.");
            boolean empty = true;
            for (LanguageModel.Builder kmeansTopic : kmeansTopics) {
              if(kmeansTopic.getTotalWeight() > 0) {
                empty = false;
                break;
              }
            }
            if(empty) continue;

            // for all valid lambdas:
            for (int lambda : lambdas) {
              ConcurrentHashMap<String, Map<String, Double>> byMethodPredictions = new ConcurrentHashMap<>();
              for (String mname : methods) {
                byMethodPredictions.put(mname, new ConcurrentHashMap<>());
              }
              for (int weight : weights) {
                byMethodPredictions.put("mix-"+weight+"-max", new ConcurrentHashMap<>());
                byMethodPredictions.put("mix-"+weight+"-min", new ConcurrentHashMap<>());
                byMethodPredictions.put("mix-"+weight+"-mean", new ConcurrentHashMap<>());
                byMethodPredictions.put("mix2-"+weight+"-max", new ConcurrentHashMap<>());
                byMethodPredictions.put("mix2-"+weight+"-min", new ConcurrentHashMap<>());
                byMethodPredictions.put("mix2-"+weight+"-mean", new ConcurrentHashMap<>());
              }
              dataset.tf10Queries.keySet().stream().forEach((clueId) -> {
                String query = dataset.tf10Queries.get(clueId);
                if(fullDocQuery) {
                  query = dataset.fullText.get(clueId);
                }

                String finalQuery = query;
                List<String> qterms = cachedQuery.computeIfAbsent(clueId, missing -> {
                  TagTokenizer tok = new TagTokenizer();
                  List<String> useful = new ArrayList<>();

                  for (String term : new HashSet<>(tok.tokenize(finalQuery).terms)) {
                    if(stopwords.contains(term) || term.length() < 3) continue;
                    useful.add(term);
                  }
                  return useful;
                });

                Map<Integer, StreamingStats> mixModelPredictions = new HashMap<>();
                Map<Integer, StreamingStats> mixModelPredictions2 = new HashMap<>();
                StreamingStats perModelPredictions = new StreamingStats();
                StreamingStats expModelPredictions = new StreamingStats();
                //StreamingStats mixModelPredictions = new StreamingStats();
                double isControvScore = logOddsScore(lambda / 100.0, globalCLM, index, qterms);
                for (LanguageModel.Builder controversy : kmeansTopics) {
                  if(controversy.getTotalWeight() <= 0) continue; // drop from consideration
                  double prediction = logOddsScore(lambda / 100.0, controversy, index, qterms);
                  if(Double.isNaN(prediction)) continue; // drop from consideration

                  perModelPredictions.push(prediction);
                  expModelPredictions.push(Math.exp(prediction));
                  for (int weight : weights) {
                    double f = weight / 100.0;
                    mixModelPredictions.computeIfAbsent(weight, ign -> new StreamingStats()).push(f*prediction + (1-f) *isControvScore);
                    mixModelPredictions2.computeIfAbsent(weight, ign -> new StreamingStats()).push(f*Math.exp(prediction) + (1-f) *Math.exp(isControvScore));
                  }
                }

                //boolean expected = dataset.judgments.get(clueId).isControversial();
                byMethodPredictions.get("max").put(clueId, perModelPredictions.getMax());
                byMethodPredictions.get("min").put(clueId, perModelPredictions.getMin());
                byMethodPredictions.get("mean").put(clueId, perModelPredictions.getMean());
                byMethodPredictions.get("exp-max").put(clueId, expModelPredictions.getMax());
                byMethodPredictions.get("exp-min").put(clueId, expModelPredictions.getMin());
                byMethodPredictions.get("exp-mean").put(clueId, expModelPredictions.getMean());
                for (int weight : weights) {
                  byMethodPredictions.get("mix-"+weight+"-max").put(clueId, mixModelPredictions.get(weight).getMax());
                  byMethodPredictions.get("mix-"+weight+"-min").put(clueId, mixModelPredictions.get(weight).getMin());
                  byMethodPredictions.get("mix-"+weight+"-mean").put(clueId, mixModelPredictions.get(weight).getMean());
                  byMethodPredictions.get("mix2-"+weight+"-max").put(clueId, mixModelPredictions2.get(weight).getMax());
                  byMethodPredictions.get("mix2-"+weight+"-min").put(clueId, mixModelPredictions2.get(weight).getMin());
                  byMethodPredictions.get("mix2-"+weight+"-mean").put(clueId, mixModelPredictions2.get(weight).getMean());
                }
                //scores.println(clueId + "\t" + prediction);
                //naiveBayes.update(predict, expected);
              });
              for (String m : byMethodPredictions.keySet()) {
                allPredictions.put(new HyperParam(depth, numTopics, "kmeans("+seed+")", lambda, m), byMethodPredictions.get(m));
              }
              System.out.println("\tGenerated all Predictions depth="+depth+" kmeans("+seed+")="+numTopics+" lambda="+lambda);
            }
          }
        }
        System.out.println("Generated all Predictions k="+depth);
      }
    }
    System.out.println("Generated all Predictions.");

    // Start search for best parameter settings per-split:
    Parameters saved = Parameters.create();
    Map<String, Double> testPredictions = new HashMap<>();


    for (ControversyDataset.DatasetSplit split : dataset.splits) {
      List<HyperParam> trainedValues = new ArrayList<>();
      //List<Pair<Integer,Double>> depthAUCPairs = new ArrayList<>();
      for (HyperParam hp : allPredictions.keySet()) {
        Map<String, Double> scores = allPredictions.get(hp);
        if(scores.isEmpty()) continue;
        List<Pair<Boolean, Double>> toEval = new ArrayList<>();
        for (String clueId : split.train) {
          if(!dataset.judgments.containsKey(clueId)) continue;
          Double score = scores.get(clueId);
          if(score == null) {
            System.err.println(split.id+" NULL: "+clueId);
            continue;
          }
          boolean judgment = dataset.judgments.get(clueId).isControversial();
          toEval.add(Pair.of(judgment, score));
        }
        double thisCutoff = tuneAccuracy ? AUC.maximizeAccuracy(toEval) : AUC.maximizeF1(toEval);
        double thisEval = tuneAccuracy ? BinaryClassifierInfo.computeAccuracy(toEval, thisCutoff) : AUC.compute(toEval);
        trainedValues.add(new HyperParam(hp, thisEval, thisCutoff));
      }

      // find the best hyper-parameters
      trainedValues.sort((lhs, rhs) -> -Double.compare(lhs.score, rhs.score));

      if(argp.get("showBestK", true)) {
        System.out.println("Best-K:");
        for (HyperParam hyperParam : ListFns.take(trainedValues, 10)) {
          System.out.println("\t" + hyperParam + " AUC=" + hyperParam.score);
        }
      }

      HyperParam best = trainedValues.get(0);
      double bestScore = best.score;
      double cutoff = best.cutoff;
      System.out.println(split.id+" best " +(tuneAccuracy ? "ACC" : "AUC")+ ": "+bestScore+" "+best.toString());

      // save deep information for learning to rank approach.
      Parameters trainP = Parameters.create();
      Parameters testP = Parameters.create();
      for (String clueId : split.test) {
        double value = allPredictions.get(best).get(clueId) - cutoff;
        testP.put(clueId, value);
        testPredictions.put(clueId, value);
      }
      for (String clueId : split.train) {
        trainP.put(clueId, allPredictions.get(best).get(clueId));
      }
      saved.put(split.id, Parameters.parseArray(
          "depth", best.depth,
          "method", best.method,
          "topicsMethod", best.topicsMethod,
          "topics", best.numTopics,
          "cutoff", best.cutoff,
          (tuneAccuracy ? "ACC" : "AUC"), best.score,
          "train", trainP, "test", testP
      ));
    }
    System.out.println("Tuned all folds.");

    IO.spit(saved.toString(), out.child("kclm.trained."+fileName+".json"));

    List<Pair<Boolean, Double>> forAUC = new ArrayList<>();
    BinaryClassifierInfo naiveBayes = new BinaryClassifierInfo();
    try (PrintWriter scores = IO.openPrintWriter(out.childPath("/kclm.trained."+fileName+".tsv"))) {
      for (Map.Entry<String, Double> kv : testPredictions.entrySet()) {
        String clueId = kv.getKey();
        if(!dataset.judgments.containsKey(clueId)) continue;
        double prediction = kv.getValue();
        boolean expected = dataset.judgments.get(clueId).isControversial();
        scores.println(clueId + "\t" + prediction);
        forAUC.add(Pair.of(expected, prediction));
      }
    }
    naiveBayes.update(forAUC, 0); // cutoff should have been subtracted off
    System.out.println("TEST AUC: "+AUC.compute(forAUC));
    System.out.println("NAIVE-BAYES: "+naiveBayes);

    if("web450".equals(argp.get("datasetName", ""))) {
      Parameters p1 = argp.clone();
      p1.put("datasetName", "web150");
      ControversyDataset web150 = new ControversyDataset(p1);

      List<Pair<Boolean, Double>> web150AUC = new ArrayList<>();
      BinaryClassifierInfo web150Info = new BinaryClassifierInfo();
      List<Pair<Boolean, Double>> clue303AUC = new ArrayList<>();
      BinaryClassifierInfo clue303Info = new BinaryClassifierInfo();

      for (Map.Entry<String, Double> kv : testPredictions.entrySet()) {
        String clueId = kv.getKey();
        double prediction = kv.getValue();
        boolean expected = dataset.judgments.get(clueId).isControversial();
        if(web150.judgments.containsKey(clueId)) {
          web150AUC.add(Pair.of(expected, prediction));
        } else {
          clue303AUC.add(Pair.of(expected, prediction));
        }
      }
      web150Info.update(web150AUC, 0);
      clue303Info.update(clue303AUC, 0);

      System.out.println("Web150 AUC: "+AUC.compute(web150AUC));
      System.out.println("Web150 Info: "+web150Info);
      System.out.println("Clue303 AUC: "+AUC.compute(clue303AUC));
      System.out.println("Clue303 Info: "+clue303Info);
    }
  }

  private List<LanguageModel.Builder> createKMeansTopics(Random rand, int numTopics, int ncDocs, List<LanguageModel.Builder> controversyModels) {
    if(numTopics == ncDocs) {
      return controversyModels;
    }
    List<LanguageModel.Builder> outputModels = new ArrayList<>(numTopics);
    for (int i = 0; i < numTopics; i++) {
      outputModels.add(new LanguageModel.HashMapLanguageModel());
    }

    // Select our K means randomly:
    ReservoirSampler<LanguageModel.Builder> sampler = new ReservoirSampler<>(rand, numTopics);
    sampler.addAll(controversyModels);
    List<Set<String>> randomK = ListFns.map(sampler, LanguageModel.Builder::getVocabulary);

    // do single-pass K-means algorithm with Jaccard-Index as similarity:
    for (LanguageModel.Builder doc : controversyModels) {
      Set<String> terms = doc.getVocabulary();
      TopKHeap<TopKHeap.Weighted<Integer>> bestTopics = new TopKHeap<>(1);
      for (int topicIndex = 0; topicIndex < randomK.size(); topicIndex++) {
        Set<String> topic = randomK.get(topicIndex);
        bestTopics.offer(new TopKHeap.Weighted<>(SetFns.jaccardIndex(topic, terms), topicIndex));
      }
      int bestTopic = bestTopics.get(0).object;
      // add this document to this output model.
      outputModels.get(bestTopic).increment(doc);
    }


    return outputModels;
  }


  /**
   * Actually calculate the log-odds score for a query string.
   * @param lambda smoothing param
   * @param contrLanguageModel positive language model (hash map)
   * @param index background/general/non-controversial language model as Galago Index
   * @param qterms tokens to score
   * @return log odds (log P(C|d) - log P(NC | d))
   */
  protected static double logOddsScore(double lambda, LanguageModel contrLanguageModel, LanguageModel index, List<String> qterms) {

    double collectionLength = index.getTotalWeight();
    double length = contrLanguageModel.getTotalWeight();
    if(length == 0) return Double.NaN;

    double cdProb = 0;
    double bgdProb = 0;
    for (String qterm : qterms) {
      double tf = contrLanguageModel.getTermWeight(qterm);
      double cf = index.getTermWeight(qterm);
      if (cf == 0) continue;
      double bgProb = cf / collectionLength;
      assert (!Double.isNaN(bgProb));

      double controvProb = lambda * (tf / length) + (1 - lambda) * bgProb;
      assert (!Double.isNaN(controvProb)) : "Issue with : "+Parameters.parseArray("lambda", lambda, "tf", tf, "length", length, "bgProb", bgProb).toString();

      cdProb += Math.log(controvProb);
      bgdProb += Math.log(bgProb);
      assert (!Double.isNaN(cdProb));
      assert (!Double.isNaN(bgdProb));
    }
    return (cdProb - bgdProb);
  }



  Cache<String, TObjectIntHashMap<String>> docCache = Caffeine.newBuilder().maximumSize(100_000).build();

  public TObjectIntHashMap<String> getDocLM(String docName, DocumentStore index) {
    return docCache.get(docName, docN -> {
      List<String> terms = index.lookupTermsForDocument(docN);

      TObjectIntHashMap<String> thisDocLM = new TObjectIntHashMap<>();

      if(terms.size() == 0) {
        return thisDocLM;
      }

      for (String term : terms) {
        if (stopwords.contains(term) || term.length() <= 3) continue;
        thisDocLM.adjustOrPutValue(term, 1, 1);
      }

      return thisDocLM;
    });
  }
  /**
   * Stick into "contrLanguageModel" terms from all documents in list "ids" from index "index" excepting the "stopwords".
   * @param ids
   * @param contrLanguageModel
   * @param index
   * @throws IOException
   */
  private void createLanguageModel(List<String> ids, LanguageModel.Builder contrLanguageModel, DocumentStore index) throws IOException {
    final HashSet<String> missing = new HashSet<>();
    for (String docName : ids) {
      TObjectIntHashMap<String> docLM = getDocLM(docName, index);
      if(docLM.isEmpty()) {
        missing.add(docName);
      }
      docLM.forEachEntry((k, count) -> {
        contrLanguageModel.increment(k, count);
        return true;
      });
    }
    System.out.println("Missing: " + missing.size()+" "+missing);
  }

  /**
   * Run a query, or collect M, D, or C scores for up to depth documents.
   * @param index galago index to run queries on (or wiki index if method doesn't start with index:
   * @param depth the K number of top documents to take, returned in order
   * @param method  the method by which to collect scores, try "mscore", "dscore", or "cscore"
   * @param rawData the directory which contains "MScore.txt", "DScore.txt", or "CScore.txt" as needed.
   * @param ids the list to save the wiki or index ids to in rank order.
   * @throws IOException
   */
  private void collectLanguageModelDocuments(DocumentStore index, int depth, String method, Directory rawData, List<String> ids) throws IOException {
    if(method.startsWith("index:")) {
      ids.addAll(index.findDocumentsForQuery(argp.get("model", "combine"), argp.get("query", "controversy"), depth));
      return;
    }

    switch(method) {
      case "mscore": {
        TopKHeap<TopKHeap.Weighted<String>> topMScores = new TopKHeap<>(depth);
        System.out.println("Start M");
        int above_84930 = 0;
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("MScore.txt"))) {
          for (String line : lines) {
            String[] data = line.split("\t");
            if(data.length < 2) continue;

            double score = Double.parseDouble(data[0]);
            if(score <= 0) continue;
            if(score >= 84930) above_84930++;

            String title = data[1].trim();
            topMScores.offer(new TopKHeap.Weighted<>(score, title));
          }
          System.out.println("for M=84930, K="+above_84930);
        }
        System.out.println("Total Non-Zero M-Score: "+topMScores.getTotalSeen());
        StreamingStats mscoreInfo = new StreamingStats();
        for (TopKHeap.Weighted<String> wt : topMScores.getSorted()) {
          ids.add(wt.object);
          mscoreInfo.push(wt.weight);
        }
        System.out.println(mscoreInfo);

      } break;
      case "dscore": {
        System.out.println("Start D");
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("DScore.txt"))) {
          for (String line : lines) {
            ids.add(line.trim());
          }
        }
        System.out.println("Total D-Score: "+ids.size());
      } break;
      case "cscore": {
        TopKHeap<TopKHeap.Weighted<String>> topCScores = new TopKHeap<>(depth);
        System.out.println("Start C");
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("CScore.txt"))) {
          for (String line : lines) {
            String[] data = line.split("\t");
            if(data.length < 2) continue;

            try {
              String title = data[0].trim();
              double score = Double.parseDouble(data[1]);
              if (score <= 0) continue;

              topCScores.offer(new TopKHeap.Weighted<>(score, title));
            } catch (NumberFormatException nfe) {
              System.err.println(line);
            }
          }
        }
        System.out.println("Total Non-Zero C-Score: "+topCScores.getTotalSeen());
        for (TopKHeap.Weighted<String> wt : topCScores.getSorted()) {
          ids.add(wt.object);
        }
      }
      break;
      default: throw new UnsupportedOperationException(method);
    }
  }
}
