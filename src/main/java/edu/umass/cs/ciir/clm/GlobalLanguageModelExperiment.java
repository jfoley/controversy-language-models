package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.TopKHeap;
import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.math.StreamingStats;
import ciir.jfoley.chai.string.StrUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.util.WordLists;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author jfoley
 */
public class GlobalLanguageModelExperiment {
  public static List<Integer> depths = new ArrayList<>(Arrays.asList(1,10,25,50,75,100,150,200,250,1000,2000,2500,3000,4000,5000,7500,10000,85000));
  public static List<Integer> lambdas = Arrays.asList(5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95);


  protected final Parameters argp;
  public ControversyDataset dataset;
  public Set<String> stopwords;

  public GlobalLanguageModelExperiment(Parameters argp) throws IOException {
    this.argp = argp;
    this.dataset = new ControversyDataset(argp);
    this.stopwords = WordLists.getWordListOrDie("smart");
  }

  /**
   * Class that lets us store a "lambda", and a "K" as a key (stores measure value and cutoff if need be, but doesn't hash them).
   * So we can store documents and language models for each K,lambda param.
   */
  public static class HyperParam {
    final int depth;
    final int lambda;
    final double score;
    final double cutoff;

    public HyperParam(int depth, int lambda, double score, double cutoff) {
      this.depth = depth;
      this.lambda = lambda;
      this.score = score;
      this.cutoff = cutoff;
    }

    public HyperParam(int depth, int lambda) {
      this.depth = depth;
      this.lambda = lambda;
      this.score = 0;
      this.cutoff = 0;
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof HyperParam) {
        HyperParam rhs = (HyperParam) other;
        return this.depth == rhs.depth && this.lambda == rhs.lambda;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return Integer.hashCode(this.depth) ^ Integer.hashCode(this.lambda);
    }
  }

  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);
    GlobalLanguageModelExperiment exp = new GlobalLanguageModelExperiment(argp);
    exp.run();
  }

  public DocumentStore openIndex(String path) {
    try {
      return new DocumentStore.GalagoDocumentStore(new LocalRetrieval(path, argp));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void run() throws IOException {
    //argp.set("dataset", "/home/jfoley/code/controversyNews/");
    String wikiSourceIndex = argp.get("wiki", "/mnt/scratch3/jfoley/dbpedia.galago");
    String outputDir = argp.get("outputDir", "output_"+dataset.getName());
    Directory out = new Directory(outputDir); // create if needed.

    String method = argp.get("method", "mscore");
    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/clue09bspam60.galago");
    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/dbpedia.galago");

    // directory that contains "MScore.txt", "CScore.txt", and "DScore.txt"
    Directory rawData = Directory.Read(argp.get("features", "/home/jfoley/code/controversyNews/clueweb"));
    final boolean fullDocQuery = argp.get("full", false);

    boolean tuneAccuracy = argp.get("tuneAccuracy", false); // else tune AUC

    String indexPath = argp.get("index", (!method.startsWith("index") ? wikiSourceIndex : StrUtil.takeAfter(method, ":")));

    String fileName = method;
    if(method.startsWith("index")) {
      fileName = StrUtil.takeBefore(new File(indexPath).getName(), ".galago");
      String query = argp.get("query", "controversy");
      if(!"controversy".equals(query)) {
        fileName += "." + StrUtil.preview(query,50).replaceAll("\\s+", "_");
      }
    }
    if(fullDocQuery) {
      fileName += ".full";
    }
    if(tuneAccuracy) {
      fileName += ".acc";
    }

    // first, collect all documents up to max depth.
    int maxDepth = depths.stream().mapToInt(x -> x).max().getAsInt();
    List<String> ids = new ArrayList<>(maxDepth);

    // calculate all predictions for all parameter settings, and then do search afterward.
    Map<HyperParam, Map<String, Double>> allPredictions = new HashMap<>();

    try (DocumentStore index = openIndex(indexPath)) {
      collectLanguageModelDocuments(index, maxDepth, method, rawData, ids);
      System.out.println("Collected all LM Docs.");

      // now, predict for all depths:
      for (int depth : depths) {
        LanguageModel.Builder controversy = new LanguageModel.HashMapLanguageModel();

        List<String> depthIds = ListFns.slice(ids, 0, depth);
        createLanguageModel(stopwords, depthIds, controversy, index);

        if(controversy.getTotalWeight() == 0) continue; // skip predictions if no documents

        // for all valid lambdas:
        for (int lambda : lambdas) {
          Map<String, Double> predictions = new HashMap<>();
          for (Map.Entry<String, String> kv : dataset.tf10Queries.entrySet()) {
            String clueId = kv.getKey();
            String query = kv.getValue();
            if(fullDocQuery) {
              query = dataset.fullText.get(clueId);
            }
            double prediction = logOddsScore(stopwords, lambda / 100.0, controversy, index, query);
            //boolean expected = dataset.judgments.get(clueId).isControversial();
            predictions.put(clueId, prediction);
            //scores.println(clueId + "\t" + prediction);
            //naiveBayes.update(predict, expected);
          }
          allPredictions.put(new HyperParam(depth, lambda), predictions);
          System.out.println("Generated all Predictions k="+depth+" lambda="+lambda);
        }
        System.out.println("Generated all Predictions k="+depth);
      }
    }
    System.out.println("Generated all Predictions.");

    // Start search for best parameter settings per-split:
    Parameters saved = Parameters.create();
    Map<String, Double> testPredictions = new HashMap<>();
    Map<Integer, List<Pair<Boolean, Double>>> testPredictionsByDepth = new HashMap<>();

    for (ControversyDataset.DatasetSplit split : dataset.splits) {
      List<HyperParam> trainedValues = new ArrayList<>();
      //List<Pair<Integer,Double>> depthAUCPairs = new ArrayList<>();
      for (int depth : depths) {
        for (int lambda : lambdas) {
          final HyperParam hp = new HyperParam(depth, lambda);
          Map<String, Double> scores = allPredictions.get(hp);
          if(scores == null || scores.isEmpty()) continue; // skip predictions if none
          List<Pair<Boolean, Double>> toEval = new ArrayList<>();
          for (String clueId : split.train) {
            if(!dataset.judgments.containsKey(clueId)) continue;
            Double score = scores.get(clueId);
            if(score == null) {
              System.err.println(split.id+" NULL: "+clueId);
              continue;
            }
            boolean judgment = dataset.judgments.get(clueId).isControversial();
            toEval.add(Pair.of(judgment, score));
          }
          double thisCutoff = tuneAccuracy ? AUC.maximizeAccuracy(toEval) : AUC.maximizeF1(toEval);
          double thisEval = tuneAccuracy ? BinaryClassifierInfo.computeAccuracy(toEval, thisCutoff) : AUC.compute(toEval);
          List<Pair<Boolean, Double>> pairs = testPredictionsByDepth.computeIfAbsent(depth, x -> new ArrayList<>());
          for (String id : split.test) {
            if(!dataset.judgments.containsKey(id)) continue;
            double value = allPredictions.get(hp).get(id); // - thisCutoff;
            pairs.add(Pair.of(dataset.judgments.get(id).isControversial(), value));
          }
          trainedValues.add(new HyperParam(depth, lambda, thisEval, thisCutoff));
        }
      }

      // find the best hyper-parameters
      trainedValues.sort((lhs, rhs) -> -Double.compare(lhs.score, rhs.score));
      HyperParam best = trainedValues.get(0);
      int bestDepth = best.depth;
      double bestScore = best.score;
      double cutoff = best.cutoff;
      System.out.println(split.id+" best " +(tuneAccuracy ? "ACC" : "AUC")+ ": "+bestScore+" depth="+bestDepth+" lambda="+best.lambda);

      // save deep information for learning to rank approach.
      Parameters trainP = Parameters.create();
      Parameters testP = Parameters.create();
      for (String clueId : split.test) {
        double value = allPredictions.get(best).get(clueId) - cutoff;
        testP.put(clueId, value);
        testPredictions.put(clueId, value);
      }
      for (String clueId : split.train) {
        trainP.put(clueId, allPredictions.get(best).get(clueId));
      }
      saved.put(split.id, Parameters.parseArray("train", trainP, "test", testP));
    }
    System.out.println("Tuned all folds.");

    IO.spit(saved.toString(), out.child("clm.trained."+fileName+".json"));

    List<Pair<Boolean, Double>> forAUC = new ArrayList<>();
    BinaryClassifierInfo naiveBayes = new BinaryClassifierInfo();
    try (PrintWriter scores = IO.openPrintWriter(out.childPath("/clm.trained."+fileName+".tsv"))) {
      for (Map.Entry<String, Double> kv : testPredictions.entrySet()) {
        String clueId = kv.getKey();
        if(!dataset.judgments.containsKey(clueId)) continue;
        double prediction = kv.getValue();
        boolean expected = dataset.judgments.get(clueId).isControversial();
        scores.println(clueId + "\t" + prediction);
        forAUC.add(Pair.of(expected, prediction));
      }
    }
    naiveBayes.update(forAUC, 0); // cutoff should have been subtracted off
    System.out.println("TEST AUC: "+AUC.compute(forAUC));
    System.out.println("NAIVE-BAYES: "+naiveBayes);

    if(argp.get("datasetName", "").startsWith("web450")) {
      Parameters p1 = argp.clone();
      p1.put("datasetName", "web150");
      ControversyDataset web150 = new ControversyDataset(p1);

      List<Pair<Boolean, Double>> web150AUC = new ArrayList<>();
      BinaryClassifierInfo web150Info = new BinaryClassifierInfo();
      List<Pair<Boolean, Double>> clue303AUC = new ArrayList<>();
      BinaryClassifierInfo clue303Info = new BinaryClassifierInfo();

      for (Map.Entry<String, Double> kv : testPredictions.entrySet()) {
        String clueId = kv.getKey();
        double prediction = kv.getValue();
        boolean expected = dataset.judgments.get(clueId).isControversial();
        if(web150.judgments.containsKey(clueId)) {
          web150AUC.add(Pair.of(expected, prediction));
        } else {
          clue303AUC.add(Pair.of(expected, prediction));
        }
      }
      web150Info.update(web150AUC, 0);
      clue303Info.update(clue303AUC, 0);

      System.out.println("Web150 AUC: "+AUC.compute(web150AUC));
      System.out.println("Web150 Info: "+web150Info);
      System.out.println("Clue303 AUC: "+AUC.compute(clue303AUC));
      System.out.println("Clue303 Info: "+clue303Info);
    }
  }


  /**
   * Actually calculate the log-odds score for a query string.
   * @param stopwords stopwords to skip
   * @param lambda smoothing param
   * @param contrLanguageModel positive language model (hash map)
   * @param index background/general/non-controversial language model as Galago Index
   * @param query text to tokenize & score
   * @return log odds (log P(C|d) - log P(NC | d))
   */
  protected static double logOddsScore(Set<String> stopwords, double lambda, LanguageModel contrLanguageModel, LanguageModel index, String query) {
    TagTokenizer tok = new TagTokenizer();
    List<String> qterms = new ArrayList<>(new HashSet<>(tok.tokenize(query).terms));

    double collectionLength = index.getTotalWeight();
    double length = contrLanguageModel.getTotalWeight();

    double cdProb = 0;
    double bgdProb = 0;
    for (String qterm : qterms) {
      if (stopwords.contains(qterm) || qterm.length() <= 3) continue;
      double tf = contrLanguageModel.getTermWeight(qterm);
      double cf = index.getTermWeight(qterm);
      if (cf == 0) continue;
      double bgProb = cf / collectionLength;

      double controvProb = lambda * (tf / length) + (1 - lambda) * bgProb;
      assert (!Double.isNaN(controvProb));
      assert (!Double.isNaN(bgProb));
      cdProb += Math.log(controvProb);
      bgdProb += Math.log(bgProb);
      assert (!Double.isNaN(cdProb));
      assert (!Double.isNaN(bgdProb));
    }
    return (cdProb - bgdProb);
  }



  Cache<String, TObjectIntHashMap<String>> docCache = Caffeine.newBuilder().maximumSize(100_000).build();

  /**
   * Stick into "contrLanguageModel" terms from all documents in list "ids" from index "index" excepting the "stopwords".
   * @param stopwords
   * @param ids
   * @param contrLanguageModel
   * @param index
   * @throws IOException
   */
  private void createLanguageModel(Set<String> stopwords, List<String> ids, LanguageModel.Builder contrLanguageModel, DocumentStore index) throws IOException {
    final HashSet<String> missing = new HashSet<>();
    for (String docName : ids) {

      TObjectIntHashMap<String> docLM = docCache.get(docName, docN -> {
        List<String> terms = index.lookupTermsForDocument(docN);

        TObjectIntHashMap<String> thisDocLM = new TObjectIntHashMap<>();

        if(terms.size() == 0) {
          missing.add(docN);
          return thisDocLM;
        }

        for (String term : terms) {
          if (stopwords.contains(term) || term.length() <= 3) continue;
          thisDocLM.adjustOrPutValue(term, 1, 1);
        }

        return thisDocLM;
      });

      docLM.forEachEntry((k, count) -> {
        contrLanguageModel.increment(k, count);
        return true;
      });
    }
    System.out.println("Missing: " + missing.size()+" "+missing);
  }

  /**
   * Run a query, or collect M, D, or C scores for up to depth documents.
   * @param index galago index to run queries on (or wiki index if method doesn't start with index:
   * @param depth the K number of top documents to take, returned in order
   * @param method  the method by which to collect scores, try "mscore", "dscore", or "cscore"
   * @param rawData the directory which contains "MScore.txt", "DScore.txt", or "CScore.txt" as needed.
   * @param ids the list to save the wiki or index ids to in rank order.
   * @throws IOException
   */
  private void collectLanguageModelDocuments(DocumentStore index, int depth, String method, Directory rawData, List<String> ids) throws IOException {
    if(method.startsWith("index:")) {
      ids.addAll(index.findDocumentsForQuery(argp.get("model", "combine"), argp.get("query", "controversy"), depth));
      return;
    }

    switch(method) {
      case "mscore": {
        TopKHeap<TopKHeap.Weighted<String>> topMScores = new TopKHeap<>(depth);
        System.out.println("Start M");
        int above_84930 = 0;
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("MScore.txt"))) {
          for (String line : lines) {
            String[] data = line.split("\t");
            if(data.length < 2) continue;

            double score = Double.parseDouble(data[0]);
            if(score <= 0) continue;
            if(score >= 84930) above_84930++;

            String title = data[1].trim();
            topMScores.offer(new TopKHeap.Weighted<>(score, title));
          }
          System.out.println("for M=84930, K="+above_84930);
        }
        System.out.println("Total Non-Zero M-Score: "+topMScores.getTotalSeen());
        StreamingStats mscoreInfo = new StreamingStats();
        for (TopKHeap.Weighted<String> wt : topMScores.getSorted()) {
          mscoreInfo.push(wt.weight);
          if(dataset.judgments.containsKey(wt.object)) continue; // skip if this is a wiki collection.
          ids.add(wt.object);
        }
        System.out.println(mscoreInfo);

      } break;
      case "dscore": {
        System.out.println("Start D");
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("DScore.txt"))) {
          for (String line : lines) {
            String wiki = line.trim();
            if(dataset.judgments.containsKey(wiki)) continue; // skip if this is a wiki collection.
            ids.add(wiki);
          }
        }
        System.out.println("Total D-Score: "+ids.size());
      } break;
      case "cscore": {
        TopKHeap<TopKHeap.Weighted<String>> topCScores = new TopKHeap<>(depth);
        System.out.println("Start C");
        try (LinesIterable lines = LinesIterable.fromFile(rawData.child("CScore.txt"))) {
          for (String line : lines) {
            String[] data = line.split("\t");
            if(data.length < 2) continue;

            try {
              String title = data[0].trim();
              double score = Double.parseDouble(data[1]);
              if (score <= 0) continue;

              topCScores.offer(new TopKHeap.Weighted<>(score, title));
            } catch (NumberFormatException nfe) {
              System.err.println(line);
            }
          }
        }
        System.out.println("Total Non-Zero C-Score: "+topCScores.getTotalSeen());
        for (TopKHeap.Weighted<String> wt : topCScores.getSorted()) {
          String wiki = wt.object;
          if(dataset.judgments.containsKey(wiki)) continue; // skip if this is a wiki collection.
          ids.add(wiki);
        }
      }
      break;
      default: throw new UnsupportedOperationException(method);
    }
  }
}
