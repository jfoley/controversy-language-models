package edu.umass.cs.ciir.clm.sim;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.math.FullStats;
import edu.umass.cs.ciir.clm.ControversyJudgment;

import java.util.*;

/**
 * @author jfoley
 */
public abstract class Simulation {
  int N;
  Random rand;
  Map<String, FullStats> measures;

  public void reset() {
    N = 10000;
    rand = new Random(13);
    measures = new HashMap<>();
  }

  protected String getMethodName() {
    return this.getClass().getSimpleName();
  }

  protected abstract void setupSimulation(Map<String, List<ControversyJudgment>> dataToTest);

  public void runSimulation(Map<String, List<ControversyJudgment>> dataToTest) {
    reset();

    setupSimulation(dataToTest);
    for (int sim = 0; sim < N; sim++) {
      List<Pair<Boolean, Double>> rankByHumanSample = new ArrayList<>(dataToTest.size());
      dataToTest.forEach((key, values) -> {
        rankByHumanSample.add(simulatePrediction(key, values));
      });

      BinaryClassifierInfo binaryInfo = new BinaryClassifierInfo();
      binaryInfo.update(rankByHumanSample, 0);

      measures.computeIfAbsent("AUC", x -> new FullStats()).push(AUC.compute(rankByHumanSample));
      measures.computeIfAbsent("P", x -> new FullStats()).push(binaryInfo.getPositivePrecision());
      measures.computeIfAbsent("R", x -> new FullStats()).push(binaryInfo.getPositiveRecall());
      measures.computeIfAbsent("F1", x -> new FullStats()).push(binaryInfo.getPositiveF1());
      measures.computeIfAbsent("Accuracy", x -> new FullStats()).push(binaryInfo.getAccuracy());
      measures.computeIfAbsent("AP", x -> new FullStats()).push(AUC.computeAP(rankByHumanSample));
    }
  }

  public Map<String, FullStats> getMeasures() {
    return measures;
  }

  public void printMeasures() {
    System.out.println("\n" + getMethodName() + " Simulation.");
    for (String measure : Arrays.asList("AUC", "P", "R", "F1", "Accuracy", "AP")) {
      FullStats info = measures.get(measure);
      System.out.printf("%s: [%1.3f ...%1.3f... %1.3f]\n",
          measure,
          info.getPercentile(5),
          info.getPercentile(50),
          info.getPercentile(95));
    }
  }

  public boolean maxTruth(List<ControversyJudgment> values) {
    double max = 0;
    for (ControversyJudgment cj : values) {
      max =  Math.max(max, cj.toRelevanceLikeRating());
    }
    return max > 0;
  }
  public boolean avgTruth(List<ControversyJudgment> values) {
    double mean = 0;
    for (ControversyJudgment cj : values) {
      mean += cj.toRelevanceLikeRating();
    }
    // equivalent:
    //return (mean / (double) values.size()) > 0;
    return mean > 0;
  }

  protected abstract Pair<Boolean, Double> simulatePrediction(String key, List<ControversyJudgment> values);
}
