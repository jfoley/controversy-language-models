package edu.umass.cs.ciir.clm.sim;

import ciir.jfoley.chai.collections.list.IntList;
import ciir.jfoley.chai.collections.util.MapFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.LinesIterable;
import edu.umass.cs.ciir.clm.ControversyDataset;
import edu.umass.cs.ciir.clm.ControversyJudgment;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author jfoley
 */
public class SimulatePredictionCeiling {

  public static void analyze(Collection<List<ControversyJudgment>> judgmentsForQueries) {

    int noAgreementInfo = 0;
    int someAgreementInfo = 0;

    final TIntIntHashMap agree = new TIntIntHashMap();
    TObjectIntHashMap<IntList> disagree = new TObjectIntHashMap<>();

    final TIntIntHashMap yagree = new TIntIntHashMap();
    TObjectIntHashMap<IntList> ydisagree = new TObjectIntHashMap<>();

    for (List<ControversyJudgment> cjs : judgmentsForQueries) {
      if(cjs.size() <= 1) {
        noAgreementInfo++;
        continue;
      }
      someAgreementInfo++;
      TIntIntHashMap freqs = new TIntIntHashMap();
      TIntIntHashMap yess = new TIntIntHashMap();
      IntList ratings = new IntList();
      IntList yn = new IntList();
      for (ControversyJudgment cj : cjs) {
        int r = (int) cj.toRelevanceLikeRating();
        ratings.push(r);
        freqs.adjustOrPutValue(r, 1, 1);
        int y = cj.isControversial() ? 1 : 0;
        yn.push(y);
        yess.adjustOrPutValue(y, 1, 1);
      }
      ratings.sort();
      yn.sort();

      if(freqs.size() == 1) {
        freqs.forEachEntry((k, v) -> {
          agree.adjustOrPutValue(k, 1, 1);
          return true;
        });
      } else {
        disagree.adjustOrPutValue(ratings, 1, 1);
      }
      if(yess.size() == 1) {
        yess.forEachEntry((k, v) -> {
          yagree.adjustOrPutValue(k, 1, 1);
          return true;
        });
      } else {
        ydisagree.adjustOrPutValue(yn, 1, 1);
      }
    }

    System.out.println("Some Agreement Info: "+someAgreementInfo+" but none on: "+noAgreementInfo);
    System.out.printf("Therefore, we only draw conclusions from %d%% of the dataset.\n",(int) ((100.0 * someAgreementInfo) / ((double) someAgreementInfo + noAgreementInfo)));

    System.out.println("Agree: "+agree);
    System.out.println("Disagree: "+disagree);

    System.out.println("Agree Y/N: "+yagree);
    System.out.println("Disagree Y/N: "+ydisagree);

  }

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    // Load clue mappings
    Map<String, String> iidToClueweb = new HashMap<>();
    {
      List<String> clueMappings = new ArrayList<>(Arrays.asList(
          "/home/jfoley/data/controversy/queriesPopulatingDataSet/seedQueriesAndResults/seedQueriesToBlekko-WikipediaTitles-Results-ClueWebOnly.txt",
          "/home/jfoley/data/controversy/queriesPopulatingDataSet/seedQueriesAndResults/seedQueriesToBlekko-BlogTrack-Results-ClueWebOnly.txt"
      ));
      for (String clueMapping : clueMappings) {
        for (String line : LinesIterable.fromFile(clueMapping).slurp()) {
          String[] row = line.split("\\s+");
          if (row.length > 1) {
            iidToClueweb.put(row[0], row[1]);
          }
        }
      }
    }

    // Load judgments:
    Map<String, List<ControversyJudgment>> judgments = new HashMap<>();
    {
      Directory judgmentDir = new Directory("/home/jfoley/data/controversy/judgments/judgments-raw-and-avg");
      List<File> jFiles = new ArrayList<>();
      jFiles.add(judgmentDir.child("judgments-pages-fullTask-2013-05-15.txt"));
      jFiles.add(judgmentDir.child("judgments-pages-lightTask-2013-05-15.txt"));

      for (File jFile : jFiles) {
        for (String crap : LinesIterable.fromFile(jFile).slurp()) {
          String[] row = crap.split("\t");
          double score = Double.parseDouble(row[1]);
          String clueId = Objects.requireNonNull(iidToClueweb.get(row[0]));
          MapFns.extendListInMap(judgments, clueId, new ControversyJudgment(clueId, score));
        }
      }
    }

    Map<String, List<ControversyJudgment>> clue303 = new HashMap<>();


    argp.put("datasetName", "clue303");
    ControversyDataset dataset = new ControversyDataset(argp);

    dataset.tf10Queries.keySet().forEach(id -> {
      clue303.put(id, judgments.get(id));
    });

    analyze(judgments.values());
    System.out.println("\n303!");
    analyze(clue303.values());

    Map<String, List<ControversyJudgment>> dataToTest = clue303;
    final boolean full = argp.get("full", false);
    if(full) {
      dataToTest = judgments;
    }

    List<Simulation> sims = Arrays.asList(
        new NaiveAgreementSimulation(),
        new SampleTruthAndPrediction(),
        new LabelConflationModel(),
        new DoubleConflationModel(),
        new MaxTruthSamplePred(),
        new AvgTruthSamplePred()
    );

    for (Simulation sim : sims) {
      sim.runSimulation(dataToTest);
      sim.printMeasures();
    }
  }

  public static class ForDHAWikiDataset {
    public static void main(String[] args) throws IOException {
      Parameters argp = Parameters.parseArgs(args);

      // Load judgments:
      Map<String, List<ControversyJudgment>> judgments = new HashMap<>();
      {
        Directory judgmentDir = new Directory("/home/jfoley/data/controversy/judgments/judgments-raw-and-avg");
        List<File> jFiles = new ArrayList<>();
        // note these say the 16th, not 15th like document judgments!
        jFiles.add(judgmentDir.child("judgments-topics-fullTask-2013-05-16.txt"));
        jFiles.add(judgmentDir.child("judgments-topics-lightTask-2013-05-16.txt"));

        for (File jFile : jFiles) {
          if(!jFile.exists()) {
            System.out.println(judgmentDir.children());
          }
          for (String crap : LinesIterable.fromFile(jFile).slurp()) {
            String[] row = crap.split("\t");
            double score = Double.parseDouble(row[1]);
            String wikiId = row[0].trim().replaceAll("\\s+", "_");
            //String clueId = Objects.requireNonNull(iidToClueweb.get(row[0]));
            MapFns.extendListInMap(judgments, wikiId, new ControversyJudgment(wikiId, score));
          }
        }
      }

      analyze(judgments.values());

      List<Simulation> sims = Arrays.asList(
          new NaiveAgreementSimulation(),
          new SampleTruthAndPrediction(),
          new LabelConflationModel(),
          new DoubleConflationModel(),
          new MaxTruthSamplePred(),
          new AvgTruthNoPred(),
          new AvgTruthSamplePred()
      );

      for (Simulation sim : sims) {
        sim.runSimulation(judgments);
        sim.printMeasures();
      }
    }

  }

  public static class ForNewDataset {
    public static void main(String[] args) throws IOException {
      List<ControversyJudgment> judgments = new ArrayList<>();
      Directory anns = Directory.Read("/home/jfoley/code/controversyNews/data/mhjang2016/release/judgments/perannotators");
      for (String annotator : Arrays.asList("a", "b", "c", "d", "e")) {
        for (String line : LinesIterable.fromFile(anns.child(annotator + ".txt")).slurp()) {
          String[] data = line.split("\t");
          if(Objects.equals(data[0], "docname")) { // skip header row still in file
            continue;
          }
          ControversyJudgment j = new ControversyJudgment(data[0], Double.parseDouble(data[1]));
          j.annotator = annotator;
          judgments.add(j);
        }
      }

      Map<String, List<ControversyJudgment>> byDocument = new HashMap<>();
      Map<String, List<ControversyJudgment>> byAnnotator = new HashMap<>();
      for (ControversyJudgment judgment : judgments) {
        MapFns.extendListInMap(byDocument, judgment.key, judgment);
        MapFns.extendListInMap(byAnnotator, judgment.annotator, judgment);
      }

      System.out.println(byDocument.size());
      System.out.println(MapFns.mapValues(byAnnotator, List::size));
      analyze(byDocument.values());

      List<Simulation> sims = Arrays.asList(
          new NaiveAgreementSimulation(),
          new SampleTruthAndPrediction(),
          new LabelConflationModel(),
          new DoubleConflationModel(),
          new MaxTruthSamplePred(),
          new AvgTruthNoPred(),
          new AvgTruthYesPred(),
          new AvgTruthRandomPred(),
          new AvgTruthSamplePred()
      );
      for (Simulation sim : sims) {
        sim.runSimulation(byDocument);
        sim.printMeasures();
      }

      System.out.println("NUM-DOCS: "+byDocument.size());
    }
  }
}
