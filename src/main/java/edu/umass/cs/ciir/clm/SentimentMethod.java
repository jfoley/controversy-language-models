package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.math.StreamingStats;

import java.util.Arrays;
import java.util.List;

/**
 * @author jfoley
 */
public class SentimentMethod {
  public static final List<String> aggregations = Arrays.asList("sum", "mean", "stddev", "mean+stddev", "max", "min");
  public static final List<String> atoms = Arrays.asList("positive", "negative", "maxabs", "mean", "ifboth", "sum");
  final String aggregation;
  final String atom;

  public SentimentMethod(String aggregation, String atom) {
    this.aggregation = aggregation;
    this.atom = atom;
  }

  @Override
  public int hashCode() {
    return aggregation.hashCode() ^ atom.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof SentimentMethod) {
      SentimentMethod rhs = (SentimentMethod) other;
      return aggregation.equals(rhs.aggregation) && atom.equals(rhs.atom);
    }
    return false;
  }

  public double atom(double pscore, double nscore) {
    switch (atom) {
      case "positive":
        return pscore;
      case "negative":
        return nscore;
      case "maxabs":
        return Math.max(pscore, nscore);
      case "mean":
        return (pscore + nscore) / 2.0;
      case "ifboth":
        return (pscore > 0 && nscore > 0) ? 1.0 : 0.0;
      case "sum":
        return pscore + nscore;
      default:
        throw new UnsupportedOperationException();
    }
  }

  /**
   * Produce a sentiment prediction for a given term vector.
   */
  public double score(List<String> terms) {
    StreamingStats source = new StreamingStats();
    for (String term : terms) {
      StreamingStats pInfo = SentiWordNetClassifier.pos.get(term);
      StreamingStats nInfo = SentiWordNetClassifier.neg.get(term);
      double pscore = pInfo == null ? 0 : pInfo.getMax(); // max across word senses.
      double nscore = nInfo == null ? 0 : nInfo.getMax(); // max across word senses.
      source.push(atom(pscore, nscore));
    }
    switch (aggregation) {
      case "sum":
        return source.getTotal();
      case "mean":
        return source.getMean();
      case "stddev":
        return source.getStandardDeviation();
      case "mean+stddev":
        return source.getMean() + source.getStandardDeviation();
      case "max":
        return source.getMax();
      case "min":
        return source.getMin();
      default:
        throw new UnsupportedOperationException();
    }
  }
}
