package edu.umass.cs.ciir.clm.sim;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.random.Sample;
import edu.umass.cs.ciir.clm.ControversyJudgment;

import java.util.List;
import java.util.Map;

/**
 * @author jfoley
 */
public class SampleTruthAndPrediction extends Simulation {
  @Override
  protected void setupSimulation(Map<String, List<ControversyJudgment>> dataToTest) {
  }

  @Override
  protected Pair<Boolean, Double> simulatePrediction(String key, List<ControversyJudgment> values) {
    ControversyJudgment truth = Sample.once(values, rand);
    ControversyJudgment prediction = Sample.once(values, rand);
    return Pair.of(truth.isControversial(), prediction.toRelevanceLikeRating());
  }
}
