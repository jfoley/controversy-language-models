package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.collections.util.MapFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.string.StrUtil;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Load all the scattered pieces of eval info into nice maps.
 * @author jfoley
 */
public class ControversyDataset {
  private String name;

  public String getName() {
    return name;
  }

  public static class DatasetSplit {
    public final String id;
    public final Set<String> train;
    public final Set<String> test;

    public DatasetSplit(String id, Set<String> train, Set<String> test) {
      this.id = id;
      this.train = train;
      this.test = test;
    }

    @Override
    public String toString() {
      return "Split "+id+" {train: "+train.size()+" test: "+test.size()+"}";
    }
  }

  /** Ids per split */
  public List<DatasetSplit> splits = new ArrayList<>();
  /** TF10 queries by clue id */
  public Map<String,String> tf10Queries = new HashMap<>();
  /** "full text" by clue id */
  public Map<String, String> fullText = new HashMap<>();
  /** judgment object by clue id */
  public Map<String, ControversyJudgment> judgments = new HashMap<>();

  public ControversyDataset(Parameters argp) throws IOException {
    this.name = argp.get("datasetName", (String) null);
    Directory controversyNews = Directory.Read(argp.get("controversyNews", "/home/jfoley/code/controversyNews"));
    if(name != null) {
      Directory clue303 = controversyNews.childDir("data").childDir("mhjang2016");
      Directory web150 = controversyNews.childDir("data").childDir("mhjang2016").childDir("release");
      Directory which;
      switch (name) {
        case "clue303":
        case "dha":
          which = clue303;
          break;
        case "web150":
          which = web150;
          break;
        case "web450":
          this.detectAndLoad(argp, clue303);
          System.err.println("Loaded: "+judgments.size());
          this.detectAndLoad(argp, web150);
          System.err.println("Loaded: "+judgments.size());
          this.splits.clear();
          this.generateFolds(10);
          return;
        case "web450ts": {
          Map<String, DatasetSplit> mergeSplits = new HashMap<>();
          this.detectAndLoad(argp, clue303);
          System.err.println("Loaded: " + judgments.size());
          this.detectAndLoad(argp, web150);
          System.err.println("Loaded: " + judgments.size());
          for (DatasetSplit split : this.splits) {
            DatasetSplit x = mergeSplits.computeIfAbsent(split.id, missing -> new DatasetSplit(missing, new HashSet<>(), new HashSet<>()));
            // combine vertically
            x.train.addAll(split.train);
            x.test.addAll(split.test);
          }
          this.splits.clear();
          this.splits.addAll(mergeSplits.values());
        } return;
        case "signal-demo":
          which = controversyNews.childDir("scripts").childDir("demo");
          break;
        default: throw new IllegalArgumentException("No such dataset="+name);
      }
      this.detectAndLoad(argp, which);
    } else {
      String dataset = argp.get("dataset", "/home/jfoley/code/controversyNews/data/mhjang2016");
      Directory inputDir = Directory.Read(dataset);
      this.name = inputDir.get().getName();
      this.detectAndLoad(argp, inputDir);
    }
  }

  private void generateFolds(int NumFolds) {
    Set<String> ids = tf10Queries.keySet();

    for (int i = 0; i < NumFolds; i++) {
      Set<String> trainItems = new HashSet<>();
      Set<String> testItems = new HashSet<>();
      // fold
      for (String id : ids) {
        int partition = (int) (Math.abs((long) id.hashCode()) % NumFolds);
        if(partition == i) {
          testItems.add(id);
        } else {
          trainItems.add(id);
        }
      }
      splits.add(new DatasetSplit("fold"+i, trainItems, testItems));
      System.out.println(ListFns.getLast(splits));
    }
  }

  private void loadSignalDataset(Directory input) throws IOException {
    Set<String> ids = new HashSet<>();
    for (String line : LinesIterable.fromFile(input.child("labels.tsv")).slurp()) {
      String[] kv = line.split("\t");
      String clueId = kv[0].trim();
      ids.add(clueId);
      double avg = Double.parseDouble(kv[1].trim());
      judgments.put(clueId, new ControversyJudgment(clueId, avg));
    }

    try (LinesIterable lines = LinesIterable.fromFile("/home/jfoley/code/controversyNews/signal_tf10.txt.gz")) {
      for (String line : lines) {
        String[] kv = line.split("\t");
        if(ids.contains(kv[0])) {
          tf10Queries.put(kv[0], kv[1]);
        }
      }
    }

    generateFolds(10);
  }

  private void loadFoldJSON(File what) throws IOException {
    Parameters foldJSON = Parameters.parseFile(what);
    for (String splitName : foldJSON.keySet()) {
      Parameters trainTest = foldJSON.getMap(splitName);
      Set<String> trainItems = new HashSet<>(trainTest.getList("train", String.class));
      Set<String> testItems = new HashSet<>(trainTest.getList("test", String.class));
      splits.add(new DatasetSplit(splitName, trainItems, testItems));
    }
  }
  private void loadClue303Dataset(Directory input) throws IOException {
    loadFoldJSON(input.child("folds.json"));

    Parameters fullDocTextJSON = Parameters.parseFile(input.child("clue_docs_cleaned.json"));
    for (String clueId : fullDocTextJSON.keySet()) {
      fullText.put(clueId, StrUtil.collapseSpecialMarks(fullDocTextJSON.getString(clueId)));
    }

    for (String line : LinesIterable.fromFile(input.child("avg_rating.txt")).slurp()) {
      String[] kv = line.split("\t");
      String clueId = kv[0].trim();
      double avg = Double.parseDouble(kv[1].trim());
      judgments.put(clueId, new ControversyJudgment(clueId, avg));
    }
    for (String line : LinesIterable.fromFile(input.child("TF10_queries.txt")).slurp()) {
      String[] kv = line.split("\t");
      String clueId = kv[0].trim();
      String query = kv[1].trim();
      tf10Queries.put(clueId, query);
    }
  }

  private void detectAndLoad(Parameters argp, Directory input) throws IOException {
    // Hack to load Signal dataset instead:
    if (input.child("labels.tsv").exists()) {
      loadSignalDataset(input);
    } else if(input.child("wiki_topic_annotations.tsv").exists()) {
      loadWikiTruthDataset(argp, input);
    } else if(input.child("tf10queries.txt").exists()) {
      loadWeb150Dataset(input);
    } else {
      loadClue303Dataset(input);
    }

    // clean
    tf10Queries = MapFns.mapValues(tf10Queries, StrUtil::collapseSpecialMarks);
    fullText = MapFns.mapValues(fullText, StrUtil::collapseSpecialMarks);
  }

  private void loadWeb150Dataset(Directory input) throws IOException {
    loadFoldJSON(input.child("folds.json"));

    File ratingsFile = input.childDir("judgments").child("avg_ratings.txt");
    //File ratingsFile = input.childDir("judgments").child("min_ratings.txt");
    //File ratingsFile = input.childDir("judgments").childDir("perannotators").child("e.txt");

    for (String line : LinesIterable.fromFile(ratingsFile).slurp()) {
      if(line.trim().isEmpty()) continue;
      String[] kv = line.split("\t");
      String clueId = kv[0].trim();
      if("page_controversy".equals(kv[1].trim())) continue;
      double avg = Double.parseDouble(kv[1].trim());
      judgments.put(clueId, new ControversyJudgment(clueId, avg));
    }
    for (String line : LinesIterable.fromFile(input.child("tf10queries.txt")).slurp()) {
      if(line.trim().isEmpty()) continue;
      String[] kv = line.split("\t");
      String clueId = kv[0].trim();
      String query = kv.length > 1 ? kv[1].trim() : "";
      tf10Queries.put(clueId, query);
    }
    TagTokenizer tok = new TagTokenizer();
    input.childDir("docs").children().forEach(fp -> {
      String name = fp.getName();
      if(judgments.containsKey(name)) {
        try {
          fullText.put(name, StrUtil.join(tok.tokenize(IO.slurp(fp)).terms));
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
  }

  private void loadWikiTruthDataset(Parameters argp, Directory input) throws IOException {
    Set<String> ids = new HashSet<>();
    Set<String> disagree = new HashSet<>();
    for (String line : LinesIterable.fromFile(input.child("wiki_topic_annotations.tsv")).slurp()) {
       try {
         String[] kv = line.split("\t");
         String wikiId = kv[0].trim().replaceAll("\\s+", "_");
         ids.add(wikiId);
         int label = Integer.parseInt(kv[1].trim());
         ControversyJudgment old = judgments.get(wikiId);
         if (old != null && (old.isControversial() != (label > 0))) {
           System.err.println("Duplicate Judgment Warning: " + wikiId + " " + old.isControversial() + " " + (label > 0));
           disagree.add(wikiId);
           continue;
         }
         // if no disagreement
         judgments.put(wikiId, new ControversyJudgment(wikiId, label > 0));
       } catch (ArrayIndexOutOfBoundsException aioobe) {
         System.err.println("ArrayOOB: "+line);
       }
    }
    System.err.println("Disagreement Wiki Pages: "+disagree);

    if(input.child("tf10.tsv").exists()) {
      try (LinesIterable lines = LinesIterable.fromFile(input.child("tf10.tsv"))) {
        for (String line : lines) {
          String[] kv = line.split("\t");
          if (ids.contains(kv[0])) {
            tf10Queries.put(kv[0], kv[1]);
          }
        }
      }
    }
    if(input.child("full.tsv").exists()) {
      try (LinesIterable lines = LinesIterable.fromFile(input.child("full.tsv"))) {
        for (String line : lines) {
          String[] kv = line.split("\t");
          if (ids.contains(kv[0])) {
            fullText.put(kv[0], kv[1]);
          }
        }
      }
    }

    String filterFile = argp.get("dhaFilter", (String) null);
    if(filterFile != null) {
      HashSet<String> names = new HashSet<>();
      for (String name : LinesIterable.fromFile(filterFile).slurp()) {
        if(name.startsWith("Talk:")) {
          continue;
        }
        names.add(name.replaceAll("\\s+", "_"));
      }
      System.out.println("Non-DHA/HODA Set: "+names.size()+" of "+tf10Queries.size());

      for (String name : names) {
        tf10Queries.remove(name);
        fullText.remove(name);
        judgments.remove(name);
      }
    }

    generateFolds(10);
  }

}
