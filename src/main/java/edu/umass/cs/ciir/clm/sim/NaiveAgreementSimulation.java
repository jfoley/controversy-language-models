package edu.umass.cs.ciir.clm.sim;

import ciir.jfoley.chai.collections.Pair;
import edu.umass.cs.ciir.clm.ControversyJudgment;
import gnu.trove.map.hash.TDoubleIntHashMap;

import java.util.List;
import java.util.Map;

/**
 * This one's awfully pessimistic since it converts predictions to boolean.
 * @author jfoley
 */
public class NaiveAgreementSimulation extends Simulation {
  private double fraction;

  @Override
  protected String getMethodName() {
    return String.format("NaiveAgreementSimulation f=%1.3f", fraction);
  }

  @Override
  public void setupSimulation(Map<String, List<ControversyJudgment>> dataToTest) {
    int agree = 0;
    int total = 0;
    for (List<ControversyJudgment> kv : dataToTest.values()) {
      // don't learn anything from agreement when there's only one label for a document!
      if(kv.size() <= 1) {
        continue;
      }

      TDoubleIntHashMap distr = new TDoubleIntHashMap();
      for (ControversyJudgment controversyJudgment : kv) {
        distr.adjustOrPutValue(controversyJudgment.isControversial() ? 1 : 0, 1, 1);
      }
      if (distr.size() == 1) {
        agree++;
      }
      total++;
    }
    // swap-fraction!
    this.fraction = agree / (double) total;
  }

  @Override
  protected Pair<Boolean, Double> simulatePrediction(String key, List<ControversyJudgment> values) {
    boolean truth = avgTruth(values);

    double weight;
    double coin = rand.nextDouble();
    if (coin < fraction) {
      // don't swap
      if (truth) {
        weight = 1;
      } else {
        weight = 0;
      }
    } else {
      //swap
      if (truth) {
        weight = 0;
      } else {
        weight = 1;
      }
    }

    return Pair.of(truth, weight);
  }
}
