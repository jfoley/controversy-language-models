package edu.umass.cs.ciir.clm.sim;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.random.Sample;
import edu.umass.cs.ciir.clm.ControversyJudgment;

import java.util.List;

/**
 * @author jfoley
 */
public class DoubleConflationModel extends LabelConflationModel {

  @Override
  protected Pair<Boolean, Double> simulatePrediction(String key, List<ControversyJudgment> values) {
    ControversyJudgment truth = Sample.once(values, rand);
    // both the truth value and the prediction undergo the noisy conflation problem here, perhaps better modeling ambiguity.
    ControversyJudgment possibleTruth = Sample.once(conflationModel.get(truth.toRelevanceLikeRating()), rand);
    ControversyJudgment prediction = Sample.once(conflationModel.get(truth.toRelevanceLikeRating()), rand);
    return Pair.of(possibleTruth.isControversial(), prediction.toRelevanceLikeRating());
  }
}
