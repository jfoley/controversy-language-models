package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.math.FullStats;
import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.time.Debouncer;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.util.WordLists;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author jfoley
 */
public class QueryTopicBasedControversyExperiment {
  public static List<Integer> depths = new ArrayList<>(Arrays.asList(1,5,10,25,50,75,100,150,200)); //,5000));
  public static List<Integer> lambdas = Arrays.asList(5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95); // fast lambda selections

  protected final Parameters argp;
  public ControversyDataset dataset;
  public Set<String> stopwords;


  public QueryTopicBasedControversyExperiment(Parameters argp) throws IOException {
    this.argp = argp;
    this.dataset = new ControversyDataset(argp);
    this.stopwords = WordLists.getWordListOrDie("smart");
  }

  /**
   * Class that lets us store a "lambda", and a "K" as a key (stores measure value and cutoff if need be, but doesn't hash them).
   * So we can store documents and language models for each K,lambda param.
   */
  public static class HyperParam {
    final int depth;
    final int lambda;
    final String method;
    final double score;
    final double cutoff;

    public HyperParam(HyperParam original, double score, double cutoff) {
      this.depth = original.depth;
      this.lambda = original.lambda;
      this.method = original.method;
      this.score = score;
      this.cutoff = cutoff;
    }

    public HyperParam(int depth, int lambda, String method) {
      this.depth = depth;
      this.lambda = lambda;
      this.method = method;
      this.score = 0;
      this.cutoff = 0;
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof HyperParam) {
        HyperParam rhs = (HyperParam) other;
        return this.depth == rhs.depth && this.lambda == rhs.lambda && Objects.equals(this.method, rhs.method);
      }
      return false;
    }

    @Override
    public int hashCode() {
      return Integer.hashCode(this.depth) ^ Integer.hashCode(this.lambda) ^ method.hashCode();
    }

    @Override
    public String toString() {
      return String.format("HyperParam(depth=%d, lambda=%1.2f, method=%s)", depth, lambda/100.0, method);
    }
  }

  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);
    QueryTopicBasedControversyExperiment exp = new QueryTopicBasedControversyExperiment(argp);
    exp.run();
  }

  public DocumentStore openIndex(String path) {
    try {
      return new DocumentStore.GalagoDocumentStore(new LocalRetrieval(path, argp));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void run() throws IOException {
    //argp.set("dataset", "/home/jfoley/code/controversyNews/");
    String wikiSourceIndex = argp.get("wiki", "/mnt/scratch3/jfoley/dbpedia.galago");
    String outputDir = argp.get("outputDir", "output"+argp.get("datasetName", ""));
    Directory out = new Directory(outputDir); // create if needed.

    for (ControversyDataset.DatasetSplit split : dataset.splits) {
      System.out.println(split);
    }

    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/clue09bspam60.galago");
    //String method = argp.get("method", "index:/mnt/scratch3/jfoley/dbpedia.galago");
    final boolean fullDocQuery = argp.get("full", false);

    boolean tuneAccuracy = argp.get("tuneAccuracy", false); // else tune AUC

    String indexPath = argp.get("index", wikiSourceIndex);

    String fileName = StrUtil.takeBefore(new File(indexPath).getName(), ".galago");
    String controversyTerm = argp.get("query", "controversy");
    if(!"controversy".equals(controversyTerm)) {
      fileName += "." + controversyTerm;
    }
    if(fullDocQuery) {
      fileName += ".full";
    }
    if(tuneAccuracy) {
      fileName += ".acc";
    }

    // first, collect all documents up to max depth.
    int maxDepth = depths.stream().mapToInt(x -> x).max().getAsInt();

    // calculate all predictions for all parameter settings, and then do search afterward.
    Map<HyperParam, Map<String, Double>> allPredictions = new HashMap<>();

    ConcurrentHashMap<String, List<String>> cachedQuery = new ConcurrentHashMap<>();

    ConcurrentHashMap<Pair<String, Integer>, LanguageModel.Builder> negativeModels = new ConcurrentHashMap<>();
    ConcurrentHashMap<Pair<String, Integer>, LanguageModel.Builder> positiveModels = new ConcurrentHashMap<>();
    Map<String, List<String>> fullTextQueries = new HashMap<>();

    try (DocumentStore index = openIndex(indexPath)) {
      Set<String> tf10vocab = new HashSet<>();
      Set<String> fullVocab = new HashSet<>();

      // Compute all language models:
      dataset.tf10Queries.forEach((clueId, query) -> {
        TagTokenizer tok = new TagTokenizer();
        List<String> useful = new ArrayList<>();

        for (String term : new HashSet<>(tok.tokenize(query).terms)) {
          if (stopwords.contains(term) || term.length() < 3) continue;
          useful.add(term);
        }

        List<String> ftq = new ArrayList<>();
        for (String term : new HashSet<>(tok.tokenize(dataset.fullText.get(clueId)).terms)) {
          if (stopwords.contains(term) || term.length() < 3) continue;
          ftq.add(term);
        }

        // full queries:
        fullTextQueries.put(clueId, ftq);
        fullVocab.addAll(ftq);

        // tf10 queries:
        tf10vocab.addAll(useful);
        cachedQuery.put(clueId, useful);
      });


      AtomicInteger finished = new AtomicInteger();
      int total = tf10vocab.size();
      Debouncer msg = new Debouncer();

      tf10vocab.parallelStream().forEach(term -> {
        int progress = finished.getAndIncrement();
        if(msg.ready()) {
          System.err.println("# Building Term Language Models: "+msg.estimate(progress, total));
        }

        List<String> negIds = index.findDocumentsForQuery("combine", term, maxDepth);
        if(negIds.size() == 0) return;
        List<String> posIds = index.findDocumentsForQuery("combine", term+" controversy", maxDepth);

        for (int depth : depths) {
          LanguageModel.HashMapLanguageModel pos = new LanguageModel.HashMapLanguageModel();
          LanguageModel.HashMapLanguageModel neg = new LanguageModel.HashMapLanguageModel();

          buildLanguageModel(index, pos, ListFns.slice(posIds, 0, depth), fullVocab);
          buildLanguageModel(index, neg, ListFns.slice(negIds, 0, depth), fullVocab);

          negativeModels.put(Pair.of(term, depth), pos);
          positiveModels.put(Pair.of(term, depth), neg);
        }
      });

      System.out.println("Generated all Language Models.");

      // Now for each query:
      dataset.tf10Queries.forEach((clueId, query) -> {
        for (int depth : depths) {
          for (int lambda : lambdas) {
            double lf = lambda / 100.0;

            List<String> terms = cachedQuery.get(clueId);
            List<String> ftqt = fullTextQueries.get(clueId);
            if(terms == null || ftqt == null) {
              System.err.println("No such query: "+clueId);
              continue;
            }

            FullStats scores = new FullStats();
            FullStats scores2 = new FullStats();

            for (String term : terms) {
              LanguageModel pos = positiveModels.get(Pair.of(term, depth));
              LanguageModel neg = negativeModels.get(Pair.of(term, depth));
              if(pos == null || neg == null) continue;

              double bgscore = logOddsScore(lf, pos, index, ftqt);
              if(Double.isNaN(bgscore)) continue;

              // is it like the controversial model for this term?
              double qscore = logOddsScore(lf, pos, neg, ftqt);
              if(Double.isNaN(qscore)) continue;
              scores.push(qscore);
              scores2.push(qscore - bgscore);
            }

            allPredictions.computeIfAbsent(new HyperParam(depth, lambda, "max"), missing -> new HashMap<>()).put(clueId, scores.getMax());
            allPredictions.computeIfAbsent(new HyperParam(depth, lambda, "min"), missing -> new HashMap<>()).put(clueId, scores.getMin());
            allPredictions.computeIfAbsent(new HyperParam(depth, lambda, "mean"), missing -> new HashMap<>()).put(clueId, scores.getMean());
            allPredictions.computeIfAbsent(new HyperParam(depth, lambda, "bg_max"), missing -> new HashMap<>()).put(clueId, scores2.getMax());
            allPredictions.computeIfAbsent(new HyperParam(depth, lambda, "bg_min"), missing -> new HashMap<>()).put(clueId, scores2.getMin());
            allPredictions.computeIfAbsent(new HyperParam(depth, lambda, "mean"), missing -> new HashMap<>()).put(clueId, scores2.getMean());
          }
        }
      });
    }


    System.out.println("Generated all Predictions.");

    // Start search for best parameter settings per-split:
    Parameters saved = Parameters.create();
    Map<String, Double> testPredictions = new HashMap<>();


    for (ControversyDataset.DatasetSplit split : dataset.splits) {
      List<HyperParam> trainedValues = new ArrayList<>();
      //List<Pair<Integer,Double>> depthAUCPairs = new ArrayList<>();
      for (HyperParam hp : allPredictions.keySet()) {
        Map<String, Double> scores = allPredictions.get(hp);
        if(scores.isEmpty()) continue;
        List<Pair<Boolean, Double>> toEval = new ArrayList<>();
        for (String clueId : split.train) {
          if(!dataset.judgments.containsKey(clueId)) continue;
          Double score = scores.get(clueId);
          if(score == null) {
            System.err.println(split.id+" NULL: "+clueId);
            continue;
          }
          boolean judgment = dataset.judgments.get(clueId).isControversial();
          toEval.add(Pair.of(judgment, score));
        }
        double thisCutoff = tuneAccuracy ? AUC.maximizeAccuracy(toEval) : AUC.maximizeF1(toEval);
        double thisEval = tuneAccuracy ? BinaryClassifierInfo.computeAccuracy(toEval, thisCutoff) : AUC.compute(toEval);
        trainedValues.add(new HyperParam(hp, thisEval, thisCutoff));
      }

      // find the best hyper-parameters
      trainedValues.sort((lhs, rhs) -> -Double.compare(lhs.score, rhs.score));

      if(argp.get("showBestK", true)) {
        System.out.println("Best-K:");
        for (HyperParam hyperParam : ListFns.take(trainedValues, 10)) {
          System.out.println("\t" + hyperParam + " AUC=" + hyperParam.score);
        }
      }

      HyperParam best = trainedValues.get(0);
      double bestScore = best.score;
      double cutoff = best.cutoff;
      System.out.println(split.id+" best " +(tuneAccuracy ? "ACC" : "AUC")+ ": "+bestScore+" "+best.toString());

      // save deep information for learning to rank approach.
      Parameters trainP = Parameters.create();
      Parameters testP = Parameters.create();
      for (String clueId : split.test) {
        double value = allPredictions.get(best).get(clueId) - cutoff;
        testP.put(clueId, value);
        testPredictions.put(clueId, value);
      }
      for (String clueId : split.train) {
        trainP.put(clueId, allPredictions.get(best).get(clueId));
      }
      saved.put(split.id, Parameters.parseArray(
          "depth", best.depth,
          "method", best.method,
          "cutoff", best.cutoff,
          (tuneAccuracy ? "ACC" : "AUC"), best.score,
          "train", trainP, "test", testP
      ));
    }
    System.out.println("Tuned all folds.");

    IO.spit(saved.toString(), out.child("local.clm.trained."+fileName+".json"));

    List<Pair<Boolean, Double>> forAUC = new ArrayList<>();
    BinaryClassifierInfo naiveBayes = new BinaryClassifierInfo();
    try (PrintWriter scores = IO.openPrintWriter(out.childPath("/local.clm.trained."+fileName+".tsv"))) {
      for (Map.Entry<String, Double> kv : testPredictions.entrySet()) {
        String clueId = kv.getKey();
        if(!dataset.judgments.containsKey(clueId)) continue;
        double prediction = kv.getValue();
        boolean expected = dataset.judgments.get(clueId).isControversial();
        scores.println(clueId + "\t" + prediction);
        forAUC.add(Pair.of(expected, prediction));
      }
    }
    naiveBayes.update(forAUC, 0); // cutoff should have been subtracted off
    System.out.println("TEST AUC: "+AUC.compute(forAUC));
    System.out.println("NAIVE-BAYES: "+naiveBayes);

    if("web450".equals(argp.get("datasetName", ""))) {
      Parameters p1 = argp.clone();
      p1.put("datasetName", "web150");
      ControversyDataset web150 = new ControversyDataset(p1);

      List<Pair<Boolean, Double>> web150AUC = new ArrayList<>();
      BinaryClassifierInfo web150Info = new BinaryClassifierInfo();
      List<Pair<Boolean, Double>> clue303AUC = new ArrayList<>();
      BinaryClassifierInfo clue303Info = new BinaryClassifierInfo();

      for (Map.Entry<String, Double> kv : testPredictions.entrySet()) {
        String clueId = kv.getKey();
        double prediction = kv.getValue();
        boolean expected = dataset.judgments.get(clueId).isControversial();
        if(web150.judgments.containsKey(clueId)) {
          web150AUC.add(Pair.of(expected, prediction));
        } else {
          clue303AUC.add(Pair.of(expected, prediction));
        }
      }
      web150Info.update(web150AUC, 0);
      clue303Info.update(clue303AUC, 0);

      System.out.println("Web150 AUC: "+AUC.compute(web150AUC));
      System.out.println("Web150 Info: "+web150Info);
      System.out.println("Clue303 AUC: "+AUC.compute(clue303AUC));
      System.out.println("Clue303 Info: "+clue303Info);
    }
  }

  Cache<String, TObjectIntHashMap<String>> docCache = Caffeine.newBuilder().maximumSize(100_000).build();
  public TObjectIntHashMap<String> getDocLM(String docName, DocumentStore index) {
    return docCache.get(docName, docN -> {
      List<String> terms = index.lookupTermsForDocument(docN);

      TObjectIntHashMap<String> thisDocLM = new TObjectIntHashMap<>();

      if(terms.size() == 0) {
        return thisDocLM;
      }

      for (String term : terms) {
        if (stopwords.contains(term) || term.length() <= 3) continue;
        thisDocLM.adjustOrPutValue(term, 1, 1);
      }

      return thisDocLM;
    });
  }

  private void buildLanguageModel(DocumentStore index, LanguageModel.HashMapLanguageModel pos, List<String> documents, Set<String> fullVocab) {
    for (String document : documents) {
      TObjectIntHashMap<String> docLM = getDocLM(document, index);
      TObjectIntHashMap<String> counts = pos.getCounts();
      docLM.forEachEntry((term, count) -> {
        if(documents.size() > 100 && count < 10) return true;
        pos.length += count;
        if(fullVocab != null && fullVocab.contains(term)) { // only store terms that are useful to us.
          counts.adjustOrPutValue(term, count, count);
        }
        return true;
      });
    }
  }

  /**
   * Actually calculate the log-odds score for a query string.
   * @param lambda smoothing param
   * @param contrLanguageModel positive language model (hash map)
   * @param index background/general/non-controversial language model as Galago Index
   * @param qterms tokens to score
   * @return log odds (log P(C|d) - log P(NC | d))
   */
  protected static double logOddsScore(double lambda, LanguageModel contrLanguageModel, LanguageModel index, List<String> qterms) {

    double collectionLength = index.getTotalWeight();
    double length = contrLanguageModel.getTotalWeight();
    if(length == 0) return Double.NaN;

    double cdProb = 0;
    double bgdProb = 0;
    for (String qterm : qterms) {
      double tf = contrLanguageModel.getTermWeight(qterm);
      double cf = index.getTermWeight(qterm);
      if (cf == 0) continue;
      double bgProb = cf / collectionLength;
      assert (!Double.isNaN(bgProb));

      double controvProb = lambda * (tf / length) + (1 - lambda) * bgProb;
      assert (!Double.isNaN(controvProb)) : "Issue with : "+Parameters.parseArray("lambda", lambda, "tf", tf, "length", length, "bgProb", bgProb).toString();

      cdProb += Math.log(controvProb);
      bgdProb += Math.log(bgProb);
      assert (!Double.isNaN(cdProb)) : "Term: "+qterm+" leads to NaN. Issue with : "+Parameters.parseArray("lambda", lambda, "tf", tf, "length", length, "bgProb", bgProb, "controvProb", controvProb).toString();
      assert (!Double.isNaN(bgdProb));
    }
    return (cdProb - bgdProb);
  }
}
