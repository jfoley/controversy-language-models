package edu.umass.cs.ciir.clm;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.TopKHeap;
import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.math.StreamingStats;
import ciir.jfoley.chai.string.StrUtil;
import org.lemurproject.galago.core.parse.TagTokenizer;
import org.lemurproject.galago.core.parse.stem.KrovetzStemmer;
import org.lemurproject.galago.core.util.WordLists;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author jfoley
 */
public class SentiWordNetClassifier {
  public static Map<String, StreamingStats> pos = new HashMap<>();
  public static Map<String, StreamingStats> neg = new HashMap<>();
  static {
    KrovetzStemmer stemmer = new KrovetzStemmer();
    try (LinesIterable lines = LinesIterable.of(IO.resourceReader("/SentiWordNet.unigrams.tsv"))) {
      for (String line : lines) {
        String col[] = line.split("\t");
        float pscore = Float.parseFloat(col[1]);
        float nscore = Float.parseFloat(col[2]);
        String term = stemmer.stem(col[0]);

        pos.computeIfAbsent(term, ign -> new StreamingStats()).push(pscore);
        neg.computeIfAbsent(term, ign -> new StreamingStats()).push(nscore);
      }

    } catch (IOException | NumberFormatException e) {
      throw new IllegalStateException("Couldn't load SentiWordNetClassifier", e);
    }
  }

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    //argp.put("dataset", "/home/jfoley/code/controversyNews/scripts/demo");
    ControversyDataset dataset = new ControversyDataset(argp);

    TagTokenizer tok = new TagTokenizer();
    KrovetzStemmer stemmer = new KrovetzStemmer();

    Parameters saved = Parameters.create();
    for (ControversyDataset.DatasetSplit split : dataset.splits) {
      Map<SentimentMethod, List<Pair<Boolean,Double>>> methods = new HashMap<>();
      for (String aggregation : SentimentMethod.aggregations) {
        for (String atom : SentimentMethod.atoms) {
          methods.put(new SentimentMethod(aggregation, atom), new ArrayList<>());
        }
      }

      for (String id : split.train) {
        String query = dataset.fullText.get(id);
        //String query = dataset.tf10Queries.get(id);
        // get stemmed terms in tf10:
        List<String> qterms = ListFns.map(tok.tokenize(query).terms, stemmer::stem);
        final boolean truth = dataset.judgments.get(id).isControversial();
        methods.forEach((method, results) -> {
          double prediction = method.score(qterms);
          results.add(Pair.of(truth, prediction));
        });
      } // finished trains:
      TopKHeap<TopKHeap.Weighted<SentimentMethod>> methodsByAUC = new TopKHeap<>(10);
      methods.forEach((method, items) -> {
        double auc = AUC.compute(items);
        methodsByAUC.offer(new TopKHeap.Weighted<>(auc, method));
      });

      SentimentMethod bestMethod = null;
      double trainAUC = 0;
      System.out.println("Train: "+split.id);
      for (TopKHeap.Weighted<SentimentMethod> w : methodsByAUC.getSorted()) {
        if(bestMethod == null) {
          bestMethod = w.object;
          trainAUC = w.weight;
        }
        System.out.println("\t"+ String.format("%1.3f", w.weight)+"\t"+w.object.aggregation+"\t"+w.object.atom);
      }

      assert(bestMethod != null);
      Parameters trainP = Parameters.create();
      Parameters testP = Parameters.create();
      System.out.println(split);
      System.out.println("TRAIN: "+split.train.size()+" TEST: "+split.test.size());
      for (String id : split.train) {
        String query = dataset.fullText.get(id);
        List<String> qterms = ListFns.map(tok.tokenize(query).terms, stemmer::stem);
        double prediction = bestMethod.score(qterms);
        trainP.put(id, prediction);
      }
      for (String id : split.test) {
        String query = dataset.fullText.get(id);
        List<String> qterms = ListFns.map(tok.tokenize(query).terms, stemmer::stem);
        double prediction = bestMethod.score(qterms);
        testP.put(id, prediction);
      }
      saved.put(split.id, Parameters.parseArray("train", trainP, "test", testP, "method", bestMethod.atom+"/"+bestMethod.aggregation, "trainAUC", trainAUC));
    } // finished all splits:
    System.out.println("Tuned all folds.");

    IO.spit(saved.toString(), new File(argp.get("output", "sentiwordnet.trained.json")));
  }

  /** Convert to file; don't need to do this, as it's checked in. */
  public static class RawImport {
    // import raw data to map
    public static void main(String[] main) throws IOException {
      Set<String> stopwords = WordLists.getWordListOrDie("inquery");

      int positive = 0;
      int negative = 0;
      int both = 0;
      int grams = 0;
      try (PrintWriter unigramSentiment = IO.openPrintWriter("SentiWordNet.unigrams.tsv");
           LinesIterable lines = LinesIterable.fromFile("/home/jfoley/data/SentiWordNet_3.0.0_20130122.txt.gz")) {
        for (String line : lines) {
          try {
            if (line.startsWith("#") || line.trim().equals("#")) continue;
            String[] cols = line.split("\t");
            float pos = Float.parseFloat(cols[2]);
            float neg = Float.parseFloat(cols[3]);
            if (pos == 0 && neg == 0) continue;

            String[] synsetTerms = cols[4].split("\\s+");
            List<String> terms = new ArrayList<>();
            for (String synsetTerm : synsetTerms) {
              // strip the sense id off the word.
              if (synsetTerm.indexOf('_') >= 0 || synsetTerm.indexOf('-') >= 0) {
                grams++;
                continue;
              }
              String rawTerm = StrUtil.takeBefore(synsetTerm, '#');
              if (rawTerm.length() < 3 || stopwords.contains(rawTerm)) continue;

              terms.add(rawTerm);
            }
            if (terms.isEmpty()) continue;

            boolean isP = pos > 0;
            boolean isN = neg > 0;
            if (isP && isN) {
              both++;
            } else if (isN) {
              negative++;
            } else if (isP) {
              positive++;
            } else throw new IllegalStateException();

            for (String term : terms) {
              unigramSentiment.println(term + "\t" + pos + "\t" + neg);
            }
          } catch (NumberFormatException nfe) {
            System.err.println(line);
          }
        }
      }

      System.out.println("Pos: " + positive + " Neg: " + negative + " Both: " + both + " Grams Skipped: " + grams);
    }
  }
}
